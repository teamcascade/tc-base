/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */



package org.teamcascade.tcbase;

import java.io.IOException;

import org.teamcascade.tcbase.configurations.CONFIG;
import org.teamcascade.tcbase.events.PlayerEvents;
import org.teamcascade.tcbase.helper.Metrics;
import org.teamcascade.tcbase.helper.logger.LogHelper;
import org.teamcascade.tcbase.objects.TC_Meta;

import org.spout.api.Spout;
import org.spout.api.plugin.CommonPlugin;

/**
 * @author $Author: dredhorse$
 * @version $FullVersion$
 * @mainpage TC-Base, a utility library used by TeamCascade
 * <p/>
 * TC-Base is a utility library used by TeamCascade.
 */
public class TC_Base extends CommonPlugin {

	/**
	 * Meta Information of the plugin
	 */
	private TC_Meta meta;

	/**
	 * Instance of the TC_Base Plugin
	 */
	private static volatile TC_Base instance;

    /**
     * Debug log can be enabled here via Spout Debug Logging first because otherwise plugin which execute before
     * tc_base will not show debug logging for some of the classes. This will be changed back to the value from the config.
     */
    private static boolean debugLogging = Spout.debugMode();

	/**
	 * Initialize the instance
	 */
	public TC_Base () {
		instance = this;
	}


	/**
	 * This method is called during disabling of the plugin, this should hold your
	 * saving and other cleanup code.
	 */
	@Override
	public void onDisable () {
		meta.getJiraIssueCheck().killJiraIssueCheck();
		meta.getReleaseBuildCheck().killReleaseBuildCheck();
		meta.getApiCheck().killApiCheck();
		if (CONFIG.CONFIG_AUTO_SAVE.getBoolean()) {
			meta.getConfig().saveConfig();
		}

		// To change body of implemented methods use File | Settings | File Templates.
	}

	/**
	 * This method is triggered when the plugin is enabled.
	 */
	@Override
	public void onEnable () {
		meta = new TC_Meta(this, CONFIG.class);
        debugLogging = CONFIG.DEBUG_LOG_ENABLED.getBoolean();
		Spout.getEventManager().registerEvents(new PlayerEvents(meta), this);
        /**
         * Enable Metrics  https://github.com/Hidendra/mcstats.org/wiki
         */

        try {
            Metrics metrics = new Metrics(meta);
            metrics.start();
        } catch (IOException e) {
            meta.getLog().debug("Problems with contacting mcstats.org", e);
        }
        LogHelper.info("Test");
	}

	/**
	 * This method is triggered during the loading of the plugin, only implements code which is really
	 * needed before the plugin is enabled.
	 */
	@Override
	public void onLoad () {


	}

	/**
	 * This method is triggered during the reloading of the plugin, implement cleanup or other code in here to
	 * allow reloading of the plugin.
	 */
	public void onReload () {
		meta.getJiraIssueCheck().killJiraIssueCheck();
		meta.getReleaseBuildCheck().killReleaseBuildCheck();
		meta.getApiCheck().killApiCheck();
		if (CONFIG.CONFIG_AUTO_SAVE.getBoolean()){
			meta.getConfig().reloadConfig();
		} else {
			meta.getConfig().loadConfig();
		}
	}


    /**
     * Do we do debug logging?
     * @return debugLogging
     */
    public static boolean isDebugLogging() {
        return debugLogging;
    }


	/**
	 * Retrieve the instance of the TC_Base class
	 *
	 * @return  instance
	 */
	public static TC_Base getInstance () {
		if (instance == null ){
			instance = new TC_Base();
		}
		return instance;
	}

}
