/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */

package org.teamcascade.tcbase.exceptions;

import org.teamcascade.tcbase.interfaces.TC_Plugin;

import org.spout.api.exception.SpoutException;

/**
 * Exception which is thrown by the CommentConfigurationManager when the configuration is not initialized.
 * NOTE: THIS IS SEVERE AND YOU SHOULD FIGURE OUT WHY THIS HAPPENED!
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */


@SuppressWarnings("ALL")
public class ConfigNotInitializedException extends SpoutException {

    private Exception exception;

    public ConfigNotInitializedException(TC_Plugin plugin) {
        super("The configuration has not been initialized!");
        exception = null;
        plugin.getTCLogger().severe("There is no configuration available for this plugin!");
        plugin.getTCLogger().severe("This is an major error and you should notify the developer!");
    }

    public ConfigNotInitializedException(TC_Plugin plugin, Exception ex) {
        super("The configuration has not been initialized!", ex);
        exception = ex;
        plugin.getTCLogger().severe("There is no configuration available for this plugin!");
        plugin.getTCLogger().severe("The following Exception was thrown:", exception);
        plugin.getTCLogger().severe("This is an major error and you should notify the developer!");
    }


    public String getMessage() {
        return "The configuration has not been initialized!, please contact the developer as this is a major error!";
    }

    public Exception getException() {
        return exception;
    }
}
