/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */

package org.teamcascade.tcbase.exceptions;

import org.spout.api.exception.SpoutException;
import org.teamcascade.tcbase.interfaces.TC_Config_Enums;

/**
 * Exception which is thrown by the Config enum when the class which is being set is different to the
 * class which is already stored in the enum
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */


public class WrongClassException extends SpoutException {

    private TC_Config_Enums tcConfigEnumsNode;

    private Object setObject;

    public WrongClassException() {
        super("A wrong class was passed");
        tcConfigEnumsNode = null;
        setObject = null;
    }

    public WrongClassException(TC_Config_Enums tcConfigEnumsNode, Object setObject) {
        super("Class expected: " + tcConfigEnumsNode.getConfigOption().getClass() + " Class found: " + setObject.getClass() + " for " + tcConfigEnumsNode);
        this.tcConfigEnumsNode = tcConfigEnumsNode;
        this.setObject = setObject;
    }

    public String getMessage() {
        return tcConfigEnumsNode == null ? "A wrong class was passed" : "Class expected: " + tcConfigEnumsNode.getConfigOption().getClass() + " Class found: " + setObject.getClass() + " for " + tcConfigEnumsNode;
    }
}


