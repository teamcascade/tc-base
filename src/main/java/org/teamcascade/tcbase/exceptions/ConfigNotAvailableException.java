/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */


package org.teamcascade.tcbase.exceptions;

import org.teamcascade.tcbase.objects.TC_Meta;

import org.spout.api.exception.SpoutException;

/**
 * Exception which is thrown by the CommentConfigurationManager when the configuration is not available.
 * NOTE: THIS IS SEVERE AND YOU SHOULD DISABLE YOUR PLUGIN IN THIS CASE!
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */


@SuppressWarnings("ALL")
public class ConfigNotAvailableException extends SpoutException {

    private static final String NO_CONFIG = "There is no configuration available for this plugin!";
	private static final String MAJOR_ERROR = "This is an major error and you should notify the developer!";
    private Exception exception;

    public ConfigNotAvailableException(TC_Meta meta) {
        super(NO_CONFIG);
        exception = null;
        meta.getLog().severe(NO_CONFIG);
		meta.getLog().severe(MAJOR_ERROR);
    }

    public ConfigNotAvailableException(TC_Meta meta, Exception ex) {
        super(NO_CONFIG, ex);
        exception = ex;
		meta.getLog().severe(NO_CONFIG);
		meta.getLog().severe("The following Exception was thrown:", exception);
		meta.getLog().severe(MAJOR_ERROR);
    }


    public String getMessage() {
        return NO_CONFIG + "\n"+ MAJOR_ERROR;
    }

    public Exception getException() {
        return exception;
    }
}
