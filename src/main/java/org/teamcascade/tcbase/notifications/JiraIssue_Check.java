/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.notifications;

import javax.xml.parsers.DocumentBuilderFactory;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.teamcascade.tcbase.helper.file.XMLParser;
import org.teamcascade.tcbase.objects.Either;
import org.teamcascade.tcbase.objects.JiraIssue;
import org.teamcascade.tcbase.objects.TC_Meta;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.spout.api.Spout;
import org.spout.api.scheduler.Task;
import org.spout.api.scheduler.TaskPriority;

import org.spout.engine.SpoutServer;

/**
 * Will parse a specific issue search list for issues. All issues which are Blockers and are Acknowledged, Resolved (means not in official build), In Progress and Reopened will
 * show up in this search list.
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class JiraIssue_Check {

	/**
	 * Url with the search List
	 */
	private String	searchSlug =
			"http://issues.teamcascade.org/sr/jira.issueviews:searchrequest-xml/10304/SearchRequest-10304.xml?tempMax=1000";

	/**
	 * Plugin which issues are checked.
	 */
	private TC_Meta meta;

	/**
	 * Task ID
	 */
	private Task task;

    /**
     * A minute
     */
    private final static Long MINUTE = 60000L;

	/**
	 * Create a plugin specific Issue Checker
	 *
	 * @param meta information for which the Issue Checker retrieves important issues
	 */
	public JiraIssue_Check(TC_Meta meta) {
		this.meta = meta;
		searchSlug  = searchSlug.replaceAll("10304", meta.getTCManifest().getRequestID());

		if (meta.getConfig().getConfigEnum("JiraEnabled").getBoolean()) {
			autoCheck();
		}
	}

	private void announceIssue () {
		meta.getLog().debug("Running Issue check");
		JiraIssue[]	jiraIssues = issueCheck();

		if (meta.getConfig().getConfigEnum("debugLogEnabled").getBoolean()){
			//Level temp = meta.getLog().returnLogLevel();
			//meta.getLog().setLevel(Level.FINEST);

			for (JiraIssue issue: jiraIssues){
				meta.getLog().debug("JiraIssue",issue, true);
			}
			//meta.getLog().setLevel(temp);
		}
		if (jiraIssues.length > 0){
			((SpoutServer) Spout.getEngine()).broadcastMessage(meta.getConfig().getConfigEnum("JiraIssuesAnnounce").toString(),"Attention: Following issues are important for plugin " + meta.getPlugin().getName());
			meta.getLog().info("Attention: Following issues are important for plugin " + meta.getPlugin().getName());
			for (JiraIssue issue: jiraIssues){
				Either<String,String> issueMessage = prepareIssueDisplay(issue);
				((SpoutServer) Spout.getEngine()).broadcastMessage(meta.getConfig().getConfigEnum("JiraIssuesAnnounce").toString(),issueMessage.getFirst() );
				meta.getLog().info(issueMessage.getSecond());
			}
			((SpoutServer) Spout.getEngine()).broadcastMessage(meta.getConfig().getConfigEnum("JiraIssuesAnnounce").toString(),"For more information please read the logs!");
		}
	}

	/**
	 * Auto Checker, which runs every 60 minutes to check for an update
	 * @see #issueCheck()
	 * @see #announceIssue()
	 */
	private void autoCheck () {
		long schedule = meta.getConfig().getConfigEnum("JiraSchedule").getInt() * MINUTE;
		task = 	Spout.getEngine().getScheduler().scheduleSyncRepeatingTask(meta.getPlugin(), new Runnable() {

            @Override
            public void run() {
                announceIssue();
            }

        }, 0, schedule, TaskPriority.LOW);

	}

	/**
	 * Parses the Search List for issues and returns an Array with Issues
	 * @return array with Issues or empty array if no issue found
	 */
	private JiraIssue[] issueCheck () {
		final JiraIssue[] NO_ISSUES = new JiraIssue[0];
		List<JiraIssue>	notifyIssues = new ArrayList<JiraIssue>();

		try {
			URL			url      = new URL(searchSlug);
			Document	document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(url.openConnection().getInputStream());

			document.getDocumentElement().normalize();

			NodeList	nodes = document.getElementsByTagName("item");

			for (int index = 0; index < nodes.getLength(); index++) {
				Node	node = nodes.item(index);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					JiraIssue	issue   = new JiraIssue();
					Element		element = (Element) node;

					issue.setTitle(XMLParser.getTagValue("title", element));
					issue.setLink(XMLParser.getTagValue("link", element));
					issue.setDescription(XMLParser.getTagValue("description", element));
					issue.setKeyID(XMLParser.getTagValue("key", element));
					issue.setStatusID(XMLParser.getTagValue("status", element));
					issue.setResolutionID(XMLParser.getTagValue("resolution", element));
					issue.setPriority(XMLParser.getTagValue("priority", element));
					issue.setType(XMLParser.getTagValue("type", element));
					notifyIssues.add(issue);
				}
			}
		} catch (MalformedURLException e) {
			meta.getLog().severe("Problem with the URL: " + searchSlug, e);
		} catch (Exception e) {
			meta.getLog().severe("Problem with checking the Issues", e);
		}

		return notifyIssues.toArray(NO_ISSUES);
	}

	/**
	 * Prepares the Issues for display ( Type, Priority and Title) and for logging ( Type, Priority, Title, Description and Link)
	 *
	 * @param issue
	 * @return first is for display, second is for logging
	 */
	private Either<String,String> prepareIssueDisplay(JiraIssue issue){
		StringBuilder first = new StringBuilder();
		first.append(issue.getType());
		first.append(", ");
		first.append(issue.getPriority());
		first.append(" priority\n");
		first.append(issue.getTitle());
		StringBuilder second = new StringBuilder();
		second.append(first);
		second.append("\n");
		second.append(issue.getDescription().replaceAll("<.?p>",""));
		second.append("\n");
		second.append(issue.getLink());
		return new Either<String, String>(first.toString(),second.toString());
	}

	public void killJiraIssueCheck(){
		if (meta.getConfig().getConfigEnum("JiraEnabled").getBoolean()) {
			Spout.getEngine().getScheduler().cancelTask(task);
		}
	}
}
