/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.notifications;

import javax.xml.parsers.DocumentBuilderFactory;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.teamcascade.tcbase.TC_Base;
import org.teamcascade.tcbase.helper.file.XMLParser;
import org.teamcascade.tcbase.objects.BadApiIssue;
import org.teamcascade.tcbase.objects.TC_Meta;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.spout.api.Spout;
import org.spout.api.entity.Player;
import org.spout.api.scheduler.Task;
import org.spout.api.scheduler.TaskPriority;

/**
 * Will check the different API levels and broadcast a message to a certain permission group.
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class API_Check {
	/**
	 * Plugin which issues are checked.
	 */
	private TC_Meta meta;
	/**
	 * API Issues detected
	 */
	private boolean apiIssuesDetected;
	/**
	 * True if there is an issue which requires the plugin to be disabled.
	 */
	private ArrayList<String> apiKillIssueDetected = new ArrayList<String>();

	/**
	 * Enum with APIs
	 */
	public static enum API {
		TC_BASE, SPOUT, GAME
	}

	/**
	 * Issues found with the API's
	 */
	private Map<API, String> apiIssues = new HashMap<API, String>(3);

	/**
	 * The Type of the Version
	 */
	public static enum VERSION_TYPE {
		REQUIRED, ACTUAL
	}

	/**
	 * Issues found with the API's
	 */
	private Map<API, Map<VERSION_TYPE, ArrayList<Integer>>> apiVersions = new HashMap<API, Map<VERSION_TYPE, ArrayList<Integer>>>(3);
	/**
	 * Version of the Game API
	 */
	private String gameStringVersion;
	/**
	 * The Game we are depending on
	 */
	private String game;
	/**
	 * Task ID
	 */
	private Task task;
	/**
	 * A minute
	 */
	private static final Long MINUTE = 60000L;

	/**
	 * Create a plugin specific API Checker
	 * @param meta information for which the API Checker compares the API Levels
	 * @param game the game we need the API for
	 */
	public API_Check(TC_Meta meta, String game) {
		this.game = game;
		if (this.game != null) {
			// As TC_Base can't softdepend on all possible Games the plugin itself needs to make sure that it is loaded after a required game
			gameStringVersion = Spout.getPluginManager().getPlugin(game).getDescription().getVersion();
		}
		this.meta = meta;
		generateVersionInformation();
		if (meta.getConfig().getConfigEnum("ApiCheckEnabled").getBoolean()) {
			apiCheck();
			announceApiIssue(null);
			autoCheck();
		}
	}

	/**
	 * Generates the numeric version from the String Versions.
	 */
	private void generateVersionInformation() {
		Map<VERSION_TYPE, ArrayList<Integer>> versions = new HashMap<VERSION_TYPE, ArrayList<Integer>>();
		String tcbStringVersion = TC_Base.getInstance().getDescription().getVersion();
		ArrayList<Integer> tcbVersion = getNumericVersion(tcbStringVersion);
		ArrayList<Integer> tcbAPI = getNumericVersion(meta.getTCManifest().getTcBaseVersion());
		versions.put(VERSION_TYPE.ACTUAL, tcbVersion);
		versions.put(VERSION_TYPE.REQUIRED, tcbAPI);
		apiVersions.put(API.TC_BASE, versions);
		versions.clear();
		String spoutStringVersion = Spout.getAPIVersion();
		ArrayList<Integer> spoutVersion = getNumericVersion(spoutStringVersion);
		ArrayList<Integer> spoutAPI = getNumericVersion(meta.getTCManifest().getSpoutAPI());
		versions.put(VERSION_TYPE.ACTUAL, spoutVersion);
		versions.put(VERSION_TYPE.REQUIRED, spoutAPI);
		apiVersions.put(API.SPOUT, versions);
		if (game != null) {
			versions.clear();
			ArrayList<Integer> gameVersion = getNumericVersion(gameStringVersion);
			ArrayList<Integer> gameAPI = getNumericVersion(meta.getTCManifest().getGameAPI());
			versions.put(VERSION_TYPE.ACTUAL, gameVersion);
			versions.put(VERSION_TYPE.REQUIRED, gameAPI);
			apiVersions.put(API.GAME, versions);
		}
	}

	/**
	 * Create a plugin specific API Checker
	 * @param meta information for which the API Checker compares the API Levels without a Game Requirement
	 */
	public API_Check(TC_Meta meta) {
		this(meta, null);
	}

	/**
	 * Announces the API issues either via log or to the player
	 * @param player to announce to, if NULL announcement will go to the log.
	 */
	public void announceApiIssue(Player player) {
		meta.getLog().debug("Announcing API Issues");
		if (apiIssuesDetected) {
			meta.getLog().debug("apiVersions", apiVersions.size());
			for (API api : apiVersions.keySet()) {
				meta.getLog().debug("API", api);
				switch (api) {
					case TC_BASE:
						announce("TC_Base", meta.getTCManifest().getTcBaseVersion(), TC_Base.getInstance().getDescription().getVersion(), player);
						break;
					case SPOUT:
						announce("Spout", meta.getTCManifest().getSpoutAPI(), Spout.getAPIVersion(), player);
						break;
					case GAME:
						announce(game, meta.getTCManifest().getGameAPI(), gameStringVersion, player);
						break;
					default:
						break;
				}
			}
		} else {
			meta.getLog().debug("No ApiIssues detected");
		}
	}

	/**
	 * Actual announcement of API issues to log or player. If #apiKillIssueDetected is true the plugin will also be disabled.
	 * @param api name of the api
	 * @param neededVersion of the api
	 * @param foundVersion of the api
	 * @param player to send the message to, if NULL the messages will be logged.
	 */
	private void announce(String api, String neededVersion, String foundVersion, Player player) {
		if (player != null) {
			player.sendMessage("Attention: This plugin requires " + api + " Version: " + neededVersion);
			player.sendMessage("You are running: " + foundVersion);
			if (!apiKillIssueDetected.contains(api)) {
				player.sendMessage("There may be issues, you might want to make sure you use the right version");
			} else {
				player.sendMessage("There are known issues with this API, disabling plugin now!");
				player.sendMessage("Please get the right version!");
				Spout.getPluginManager().disablePlugin(meta.getPlugin());
			}
		} else {
			meta.getLog().info("Attention: This plugin requires " + api + " Version: " + neededVersion);
			meta.getLog().info("You are running: " + foundVersion);
			if (!apiKillIssueDetected.contains(api)) {
				meta.getLog().info("There may be issues, you might want to make sure you use the right version");
			} else {
				meta.getLog().severe("There are known issues with this API, disabling plugin now!");
				meta.getLog().info("Please get the right version!");
				Spout.getPluginManager().disablePlugin(meta.getPlugin());
			}
		}
	}

	/**
	 * Checks the API Issues.
	 * XxxVersion is the Version retrieved for the running API's
	 * XxxAPI is the Version retrieved from the manifest
	 * If API_CHECK_DISABLE_PLUGIN is enabled it will disable the plugin if the following conditions are met:
	 * <ul>
	 * <li>TC_BASE is lower than required or has a higher major version number</li>
	 * <li>Spout API is lower than required</li>
	 * <li>Game API is lower than required</li>
	 * </ul>
	 * </p>
	 * or when the ApiVersion.yml contains a kill switch argument.
	 * NOTE: The remote Kill option is being run it it's own thread so finding issues might be delayed.
	 * @todo version checking only works for REAL versions, neither Spout nor Vanilla have those atm.
	 */
	private void apiCheck() {
		// Check if TC_Base version differs against the version of the referenced TC_Base Plugin
		if (!meta.getTCManifest().getTcBaseVersion().equalsIgnoreCase(TC_Base.getInstance().getDescription().getVersion()) && apiCheckFailed(apiVersions.get(API.TC_BASE).get(VERSION_TYPE.ACTUAL), apiVersions.get(API.TC_BASE).get(VERSION_TYPE.REQUIRED), true)) {
			apiIssues.put(API.TC_BASE, TC_Base.getInstance().getDescription().getVersion());
			apiIssuesDetected = true;
			if (meta.getConfig().getConfigEnum("ApiCheckDisablePlugin").getBoolean()) {
				apiKillIssueDetected.add("TC_Base");
			}
		}
		// Check if Spout version differs against the version of the referenced Spout Version
		if (!meta.getTCManifest().getSpoutAPI().equalsIgnoreCase(Spout.getAPIVersion()) && apiCheckFailed(apiVersions.get(API.SPOUT).get(VERSION_TYPE.ACTUAL), apiVersions.get(API.SPOUT).get(VERSION_TYPE.REQUIRED), true)) {
			apiIssues.put(API.SPOUT, Spout.getAPIVersion());
			apiIssuesDetected = true;
			if (meta.getConfig().getConfigEnum("ApiCheckDisablePlugin").getBoolean()) {
				apiKillIssueDetected.add("Spout");
			}
		}

		// Check if the Game version differs against the version of the referenced Game Version
		meta.getLog().debug("Game", game);
		if (game != null && !meta.getTCManifest().getGameAPI().equalsIgnoreCase(gameStringVersion) && apiCheckFailed(apiVersions.get(API.GAME).get(VERSION_TYPE.ACTUAL), apiVersions.get(API.GAME).get(VERSION_TYPE.REQUIRED), true)) {
			apiIssues.put(API.GAME, Spout.getAPIVersion());
			apiIssuesDetected = true;
			if (meta.getConfig().getConfigEnum("ApiCheckDisablePlugin").getBoolean()) {
				apiKillIssueDetected.add("Game");
			}
		}
	}

	/**
	 * Auto Checker, which runs every 60 minutes to check for remote kill
	 * @see #remoteKillNecessary()
	 */
	private void autoCheck() {
		long schedule = meta.getConfig().getConfigEnum("ApiCheckSchedule").getInt() * MINUTE;
		task = Spout.getEngine().getScheduler().scheduleSyncRepeatingTask(meta.getPlugin(), new Runnable() {
			@Override
			public void run() {
				remoteKillNecessary();
			}
		}, 0, schedule, TaskPriority.LOW);
	}

	private void remoteKillNecessary() {
		BadApiIssue[] badApiIssues = getBadApiIssues(meta.getTCManifest().getGitRepoUrl() + "/raw/master/ApiVersions.xml");
		if (meta.getConfig().getConfigEnum("debugLogEnabled").getBoolean()) {
			for (BadApiIssue issue : badApiIssues) {
				meta.getLog().debug("BadApiIssue", issue, true);
			}
		}

		if (badApiIssues.length > 0) {
			String pluginVersion = meta.getPlugin().getDescription().getVersion();
			meta.getLog().debug("PluginVersion", pluginVersion);
			for (BadApiIssue issue : badApiIssues) {
				// is our plugin version in the remote kill list?
				meta.getLog().debug("Issue API", issue.getPluginVersion());
				if (issue.getPluginVersion().equals(pluginVersion)) {
					meta.getLog().debug("Found API issue");
					if (issue.justKill()) {
						// let's disable the plugin
						meta.getLog().debug("Killing plugin because of justKill");
						apiKillIssueDetected.add(meta.getPlugin().getName());
						apiIssuesDetected = true;
						announce(meta.getPlugin().getName(), pluginVersion, "an RemoteKilled Version", null);
					} else {
						meta.getLog().debug("SpoutAPI", Spout.getAPIVersion());
						meta.getLog().debug("GameAPI", gameStringVersion);
						// todo spout version checking doesn't work atm as the version is dev b2811
/*						if (checkKillVersions(API.SPOUT,issue.getBadSpoutApi())) {
							apiKillIssueDetected.add("Spout");
							apiIssuesDetected = true;
						}*/
						if (gameStringVersion != null && checkKillVersions(API.GAME, issue.getBadGameApi())) {
							apiKillIssueDetected.add("Game");
							apiIssuesDetected = true;
						}
					}
				}
			}
		}
	}

	private BadApiIssue[] getBadApiIssues(String blacklist) {
		final BadApiIssue[] NO_ISSUES = new BadApiIssue[0];
		List<BadApiIssue> notifyIssues = new ArrayList<BadApiIssue>();

		try {
			URL url = new URL(blacklist);
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(url.openConnection().getInputStream());

			document.getDocumentElement().normalize();

			NodeList nodes = document.getElementsByTagName("pluginVersion");

			for (int index = 0; index < nodes.getLength(); index++) {
				Node node = nodes.item(index);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					BadApiIssue issue = new BadApiIssue();
					Element element = (Element) node;

					issue.setPluginVersion(XMLParser.getTagValue("TC_BASE", element));
					issue.setBadSpoutApi(XMLParser.getTagValue("SPOUT", element));
					issue.setBadGameApi(XMLParser.getTagValue("GAME", element));
					notifyIssues.add(issue);
				}
			}
		} catch (MalformedURLException e) {
			meta.getLog().severe("Problem with the URL: " + blacklist, e);
		} catch (Exception e) {
			meta.getLog().severe("Problem with checking the Issues", e);
		}
		return notifyIssues.toArray(NO_ISSUES);
	}

	private boolean checkKillVersions(API api, String version) {
		boolean weNeedToKill = false;
		if (version.equalsIgnoreCase("*")) {
			weNeedToKill = true;
		} else {
			switch (api) {
				case TC_BASE:
					weNeedToKill = checkVersionFailed(getNumericVersion(version), apiVersions.get(API.TC_BASE).get(VERSION_TYPE.ACTUAL), true);
					break;
				case SPOUT:
					weNeedToKill = checkVersionFailed(getNumericVersion(version), apiVersions.get(API.SPOUT).get(VERSION_TYPE.ACTUAL), true);
					break;
				case GAME:
					weNeedToKill = checkVersionFailed(getNumericVersion(version), apiVersions.get(API.GAME).get(VERSION_TYPE.ACTUAL), true);
					break;
				default:
					break;
			}
		}
		return weNeedToKill;
	}

	/**
	 * Checks if a version (which is retrieved from the running api's is lower than the API which is required from the POM
	 * @param version actual running version of the api's
	 * @param api specified in the pom and which is at least required
	 * @param tcbase if this check is for the TC_Base version, if true and the major version is different we disable the plugin to allow deprecation
	 * @return true if the check failed
	 */
	private boolean apiCheckFailed(ArrayList<Integer> version, ArrayList<Integer> api, boolean tcbase) {
		boolean checkFailed = false;
		if (tcbase) {
			if (version.get(0) > api.get(0)) {
				checkFailed = true;
			} else {
				checkFailed = checkVersionFailed(version, api);
			}
		} else {
			checkFailed = checkVersionFailed(version, api);
		}
		return checkFailed;
	}

	/**
	 * Parses the two ArrayLists and check if one value of version is lower than api. If yes we return true.
	 * @param version to check
	 * @param api to check
	 * @return true if one value of version is lower than api
	 */
	private boolean checkVersionFailed(ArrayList<Integer> version, ArrayList<Integer> api) {
		return checkVersionFailed(version, api, false);
	}

	/**
	 * Parses the two ArrayLists and check if one value of version is lower than api. If yes we return true.
	 * @param version to check
	 * @param api to check
	 * @param lessEquals if true will check with lessEquals
	 * @return true if one value of version is lower / equals than api depending on lessEquals
	 */
	private boolean checkVersionFailed(ArrayList<Integer> version, ArrayList<Integer> api, boolean lessEquals) {
		boolean failed = false;
		int maxIndex;
		if (version.size() <= api.size()) {
			maxIndex = version.size();
		} else {
			maxIndex = api.size();
		}
		int i = 0;
		while (i < maxIndex) {
			if (lessEquals) {
				if (version.get(i) <= api.get(i)) {
					failed = true;
				}
			} else {
				if (version.get(i) < api.get(i)) {
					failed = true;
				}
			}
			i++;
		}
		return failed;
	}

	/**
	 * Parses the string version into a numeric version which is saved in an array.
	 * If the last index of tha array contains a -1 this means that there was an additional string attached to the version.
	 * By default the result will look like:  major, minor, development, -1 if string attached to version.
	 * but the length of the array is dynamic to the version passed, with always the lasted one a -1 if a string was found.
	 * @param version to convert to numericVersion
	 * @return version in numeric form
	 */
	private ArrayList<Integer> getNumericVersion(String version) {
		String[] versionArray = version.split("\\.");
		ArrayList<Integer> numericVersion = new ArrayList<Integer>();
		for (String numberString : versionArray) {
			if (numberString.matches(".*[A-Za-z].*")) {
				numericVersion.add(-1);
			} else {
				numericVersion.add(Integer.parseInt(numberString));
			}
		}
		return numericVersion;
	}

	public void killApiCheck() {
		if (meta.getConfig().getConfigEnum("ApiCheckEnabled").getBoolean() && task != null) {
			Spout.getEngine().getScheduler().cancelTask(task);
		}
	}
}
