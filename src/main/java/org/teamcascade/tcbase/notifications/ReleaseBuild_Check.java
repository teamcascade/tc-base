/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.notifications;

import javax.xml.parsers.DocumentBuilderFactory;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.teamcascade.tcbase.helper.file.XMLParser;
import org.teamcascade.tcbase.objects.ReleaseBuild;
import org.teamcascade.tcbase.objects.TC_Meta;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.spout.api.Spout;
import org.spout.api.scheduler.Task;
import org.spout.api.scheduler.TaskPriority;

import org.spout.engine.SpoutServer;

/**
 * Will get the released build information from bamboo and figures out if there is a new version available.
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class ReleaseBuild_Check {

	/**
	 * Url with the builds list
	 */
	private String	searchSlug =
			"http://builds.teamcascade.org/rss/createAllBuildsRssFeed.action?feedType=rssAll&buildKey=TCB-RB";

	/**
	 * Plugin which builds are checked.
	 */
	private TC_Meta meta;

	/**
	 * Task ID
	 */
	private Task task;

	/**
	 * Is there a newer version available?
	 */
	private boolean newVersionAvailable;

    /**
     * A minute
     */
    private final static Long MINUTE = 60000L;

	/**
	 * Create a plugin specific ReleaseBuild Checker
	 *
	 * @param meta information for which the ReleaseBuild Checker retrieves builds
	 */
	public ReleaseBuild_Check(TC_Meta meta) {
		this.meta = meta;
		searchSlug  = searchSlug.replaceAll("TCB-RB", meta.getTCManifest().getReleaseBuildID());

		if (meta.getConfig().getConfigEnum("BuildCheckEnabled").getBoolean()) {
			autoCheck();
		}
	}

	private void announceReleaseBuild() {
		meta.getLog().debug("Running Release Build check");
		ReleaseBuild[]	releaseBuilds = releaseBuildCheck();

		if (meta.getConfig().getConfigEnum("debugLogEnabled").getBoolean()){
            for (ReleaseBuild releaseBuild: releaseBuilds){
				meta.getLog().debug("ReleaseBuild",releaseBuild,true);
			}
		}
		if (releaseBuilds.length>0){
			String[] newVersion = releaseBuilds[0].getVersion().split("\\.");
			meta.getLog().debug("newVersion",releaseBuilds[0].getVersion());
			String[] oldVersion = meta.getPlugin().getDescription().getVersion().split("-")[0].split("\\.");
			meta.getLog().debug("oldVersion",meta.getPlugin().getDescription().getVersion().split("-")[0]);
            int loop;
            if (newVersion.length != oldVersion.length){
                if (newVersion.length > oldVersion.length){
                    loop = oldVersion.length;
                } else {
                    loop = newVersion.length;
                }
            } else {
                loop = newVersion.length;
            }
            for (int i = 0; i < loop; i++) {
                if (!checkVersion(newVersion[i],oldVersion[i])){
                    newVersionAvailable = true;
                }
            }
			if (newVersionAvailable){
				meta.getLog().info("Update " + releaseBuilds[0].getVersion() + " is out!");
				meta.getLog().info("Get it at: " + releaseBuilds[0].getLink());
				((SpoutServer) Spout.getEngine()).broadcastMessage(meta.getConfig().getConfigEnum("BuildCheckAnnounce").toString(),"There is an update for " + meta.getPlugin().getName() + "available");
				((SpoutServer) Spout.getEngine()).broadcastMessage(meta.getConfig().getConfigEnum("BuildCheckAnnounce").toString(),"Please check the logs for more information!");
			}
		}
	}

    /**
     * Compares the two strings, if the strings are not the same, converts them to integer
     * and checks if newString is bigger than oldString
     *
     * @param newString newVersion
     * @param oldString oldVersion
     * @return true if version the same, false if newVersion is bigger than oldVersion
     */
    private boolean checkVersion(String newString, String oldString) {
        if (!newString.equalsIgnoreCase(oldString)){
            int newValue;
            int oldValue;
            try {
                newValue = Integer.parseInt(newString);
            } catch (Exception e) {
                meta.getLog().severe("There was an issue with the new Version String",e);
                return true;
            }
            try {
                oldValue = Integer.parseInt(oldString);
            } catch (Exception e) {
                meta.getLog().severe("There was an issue with the old Version String",e);
                return true;
            }
            if (newValue>oldValue){
                return false;
            }
        }
    return true;
    }

    /**
	 * Auto Checker, which runs every 60 minutes to check for an update
	 * @see #releaseBuildCheck
	 * @see #announceReleaseBuild
	 */
	private void autoCheck () {
		long schedule = meta.getConfig().getConfigEnum("BuildCheckSchedule").getInt() * MINUTE;
		task = 	Spout.getEngine().getScheduler().scheduleSyncRepeatingTask(meta.getPlugin(), new Runnable() {

			@Override
			public void run () {
				announceReleaseBuild();
			}

		}, 0, schedule, TaskPriority.LOW);

	}

	/**
	 * Parses the Feed for Successful Builds and returns an Array with Release Builds
	 * @return array with ReleaseBuilds or empty array if no ReleaseBuilds found
	 */
	private ReleaseBuild[] releaseBuildCheck() {
		final ReleaseBuild[] noRB = new ReleaseBuild[0];
		List<ReleaseBuild> releaseBuilds = new ArrayList<ReleaseBuild>();

		try {
			URL url = new URL(searchSlug);
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(url.openConnection().getInputStream());

			document.getDocumentElement().normalize();

			NodeList nodes = document.getElementsByTagName("item");

			int index = 0;
			while (index < nodes.getLength()) {
				Node	node = nodes.item(index);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element		element = (Element) node;
					String title =  XMLParser.getTagValue("title", element);
					if (title.contains("was SUCCESSFUL :")){
						ReleaseBuild	releaseBuild   = new ReleaseBuild();
						releaseBuild.setTitle(title);
						releaseBuild.setLink(XMLParser.getTagValue("link", element));
						releaseBuild.setDescription(XMLParser.getTagValue("description", element));
						releaseBuilds.add(releaseBuild);
						index = nodes.getLength();
					}
				}
				index++;
			}
		} catch (MalformedURLException e) {
			meta.getLog().severe("Problem with the URL: " + searchSlug, e);
		} catch (Exception e) {
			meta.getLog().severe("Problem with checking the Release Builds", e);
		}

		return releaseBuilds.toArray(noRB);
	}

	/**
	 * Kill the async task for checking the ReleaseBuilds
	 */
	public void killReleaseBuildCheck (){
		if (meta.getConfig().getConfigEnum("BuildCheckEnabled").getBoolean()) {
			Spout.getEngine().getScheduler().cancelTask(task);
		}
	}
}
