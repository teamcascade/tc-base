/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.helper.logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.spout.api.Spout;
import org.spout.api.plugin.Plugin;

/**
 * Logger is a HelperClass which allows you to use the full logging levels instead of just INFO,WARNING and SEVERE.
 * It is using a workaround to accomplish this by logging the not existing levels via a custom tag and the INFO level.
 * It also adds a DEBUG log function which will even printout the module.method and line which called that function if
 * logging is done via FINER and higher.
 * <br>
 * It allows you to log to a file INSTEAD of logging to a console or to log to both.If you enable Logging to FILE
 * everything starting from CONFIG onwards will be logged to file only,including DEBUG.
 * <br>
 * On Top of that it supplies a enableMessage and disableMessage method which will log via INFO level a message.
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class Logger {
	/**
	 * DateFormat
	 */
	private String dateFormat = "yyyy-MM-dd HH:mm:ss";
	/**
	 * Line marker
	 */
	private static final String LINE = "---------------------------------------";
	/**
	 * File Separator
	 */
	private static final String FILE_SEPARATOR = "file.separator";
	/**
	 * The version of the plugin using this logger
	 */
	private String pluginVersion = "";
	/**
	 * Enable Message Formatting
	 */
	private boolean enableMessageFormatting = false;
	/**
	 * Switch being used to store enableMessageFormatting for file logging
	 * as we don't need it there
	 */
	private boolean wasMessageFormattingEnabled = true;
	/**
	 * Log to LogFile only
	 */
	private boolean logToConsoleAndFile = true;
	/**
	 * Configures Fancy Logging of Exceptions
	 */
	private boolean fancyExceptionLog = true;
	/**
	 * Enable Debug Logging
	 */
	private boolean debugLogEnabled;
	/**
	 * Enable logging to file
	 */
	private boolean enableFileLog;
	/**
	 * FileHandler for Logging
	 */
	private FileHandler fileHandler;
	/**
	 * The Logger
	 */
	private java.util.logging.Logger log;
	/**
	 * LogDirectoy
	 */
	private String logDirectory;
	/**
	 * LogFileName
	 */
	private String logFileName;
	/**
	 * The directory of the plugin configuration and logs
	 */
	private String pluginDirectory;
	/**
	 * The name of the plugin using this logger
	 */
	private String pluginName;

	/**
	 * Default Log Level = Level.CONFIG
	 */
	private Level defaultLevel = Level.CONFIG;
	/**
	 * One Hundred
	 */
	private static final int oneHundred = 100;



	/**
	 * Constructor which creates a logger and sets the pluginName and pluginDirectory Defaults
	 * This is the preferred method to use this class!
	 * @param plugin to log for
	 */
	public Logger(Plugin plugin) {
		log = plugin.getLogger();
		log.setLevel(defaultLevel);
		pluginName = plugin.getDescription().getName();
		pluginDirectory = "plugins" + System.getProperty(FILE_SEPARATOR) + pluginName
				+ System.getProperty(FILE_SEPARATOR);
		logFileName = pluginName + ".log";
		logDirectory = "logs" + System.getProperty(FILE_SEPARATOR);
		/**
		 *  Setting debug Log and Loging to File and Console to the value of the spout debugMode.
		 *  Make sure that you change those values to the right values when the config is read!
		 *  @todo change debugLogEnabled and logToConsoleAndFile to correct values from config
		 */

		debugLogEnabled = Spout.debugMode();
		logToConsoleAndFile = Spout.debugMode();
		configureFileLog(Spout.debugMode());
	}

	//~--- methods ------------------------------------------------------------

	//  Log Level Methods

	/**
	 * logs message with INFO level to console or file
	 * @param msg
	 */
	public void info(String msg) {
		log.info(formatMessage(msg));
	}

	/**
	 * logs messages with SEVERE level to console or file
	 * @param msg
	 */
	public void severe(String msg) {
		log.severe(formatMessage(msg));
	}

	/**
	 * logs messages and exceptions with SEVERE level to console or file
	 * @param msg
	 * @param ex
	 */
	public void severe(String msg, Throwable ex) {
		if (fancyExceptionLog) {
			log.severe(formatMessage(LINE));

			if (!"".equals(msg)) {
				log.severe("# " + msg);
			}

			log.severe(ex.toString());
			log.severe("#");
			for (final StackTraceElement stack : ex.getStackTrace()) {
				log.severe("\t" + stack.toString());
			}

			log.severe(formatMessage(LINE));
		} else {
			log(Level.SEVERE, formatMessage(msg), ex);
		}
	}

	/**
	 * logs messages with WARNING level to console or file
	 * @param msg
	 */
	public void warning(String msg) {
		log.warning(formatMessage(msg));
	}

	/**
	 * logs messages and exceptions with WARNING level to console or file
	 * @param msg
	 * @param ex
	 */
	public void warning(String msg, Throwable ex) {
		if (fancyExceptionLog) {
			log.warning(formatMessage(LINE));

			if (!"".equals(msg)) {
				log.warning("# " + msg);
			}

			log.warning(ex.toString());
			log.warning("#");
			for (final StackTraceElement stack : ex.getStackTrace()) {
				log.warning("\t" + stack.toString());
			}

			log.warning(formatMessage(LINE));
		} else {
			log(Level.WARNING, formatMessage(msg), ex);
		}
	}

	/**
	 * logs messages with CONFIG level to console or file
	 * @param msg
	 */
	public void config(String msg) {
		if (log.isLoggable(Level.CONFIG) && !enableFileLog) {
			log.info(formatMessage("[CONFIG] " + msg));
		} else {
			log.config(msg);
		}
	}

	/**
	 * logs messages with FINE level to console or file
	 * @param msg
	 */
	public void fine(String msg) {
		if (log.isLoggable(Level.FINE) && !enableFileLog) {
			log.info(formatMessage("[FINE] " + msg));
		} else {
			log.fine(msg);
		}
	}

	/**
	 * logs messages with FINER level to console or file
	 * @param msg
	 */
	public void finer(String msg) {
		if (log.isLoggable(Level.FINER) && !enableFileLog) {
			log.info(formatMessage("[FINER] " + msg));
		} else {
			log.finer(msg);
		}
	}

	/**
	 * logs messages with FINEST level to console or file
	 * @param msg
	 */
	public void finest(String msg) {
		if (log.isLoggable(Level.FINEST) && !enableFileLog) {
			log.info(formatMessage("[FINEST] " + msg));
		} else {
			log.finest(msg);
		}
	}

	/**
	 * will output with specified level to console or file
	 * @param level LoggerLevel = FINEST, FINER, FINE, CONFIG, INFO, WARNING, SEVERE
	 * @param msg message to output
	 * @param exception exception to output
	 */
	public void log(Level level, String msg, Throwable exception) {
		log.log(level, formatMessage(msg), exception);
	}

	//  Log Level Configuration Methods

	/**
	 * Debug Logging contains information depending on FINE, FINER, FINEST
	 * @param msg Message to display, if Level is FINE or higher will also log class, method, line of call
	 */
	public void debug(String msg) {
		debug(msg, null);
	}

    /**
     * Debug Logging which will also display all fields of the object if dumpFields is true.
     * NOTE: Using this with dumpFields = false and logging of FINE will offset the line number to this class!
     * @param msg Message to display, if Level is FINE or higher will also log class, method, line of call
     */
    public void debug(String msg, Object o, boolean dumpFields){
        if (dumpFields){
            StringBuilder newO = dumpFields(o);
            debug(msg,newO);
        } else {
          debug(msg,o);
        }
    }

	/**
	 * Debug Logging contains information depending on FINE, FINER, FINEST
	 * if Level is FINE or higher will also log class, method, line of call
	 * if Level is FINEST or higher also all non static fields with values of
	 * a passed object will be displayed. WARNING: This can be lots of information!
	 * @param msg Message to display,
	 * @param o object to add to the message in []
	 */
	public void debug(String msg, Object o) {
		if (!debugLogEnabled) {
			return;
		}

		if (log.getLevel().intValue() > Level.INFO.intValue()) {

			// we do debug logging via INFO so return if not possible
			return;
		}
        String tmpMsg = msg;
		// if log level is FINE or higher print line of code
		if (log.getLevel().intValue() <= Level.FINE.intValue()) {
			int index;

			if (o != null) {
				index = 1;
			} else {
				index = 2;
			}

			StackTraceElement[] ste = new RuntimeException().getStackTrace();
			String className = ste[index].getClassName().substring(ste[index].getClassName().lastIndexOf(".") + 1);
			String methodName = ste[index].getMethodName();
			int lineNumber = ste[index].getLineNumber();

			tmpMsg = className + "." + methodName + " line: " + lineNumber + " = " +tmpMsg;
		}

		// if log level is FINEST print all fields of the object.
		if ((o != null) && (log.getLevel().intValue() == Level.FINEST.intValue())){
            StringBuilder result = dumpFields(o);
			tmpMsg = tmpMsg + "\n"+ result;
		}

		if (o != null) {
			tmpMsg = tmpMsg + " [" + o.toString() + "]";
		}

		if (enableFileLog && !logToConsoleAndFile) {
			log.log(LogUtilityLevel.DEBUG, " " + tmpMsg);
		} else {
			info("[DEBUG] " + tmpMsg);
		}
	}

    /**
     * Method to dump all fields of an object
     * @param o to dump all fields from
     * @return all fields of an object
     */
    private StringBuilder dumpFields(Object o) {
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty("line.separator");

        result.append( o.getClass().getName() );
        result.append( " Object {" );
        result.append(newLine);

        //determine fields declared in this class only (no fields of superclass)
        Field[] fields = o.getClass().getDeclaredFields();

        //print field names paired with their values
        for ( Field field : fields  ) {
            field.setAccessible(true);
            result.append("  ");
            result.append( field.getName() );
            result.append(": ");
            try {
                result.append( field.get(o) );
            } catch ( IllegalAccessException ex ) {
                try {
                    result.append(field.get(null));
                } catch (IllegalAccessException e) {
                    result.append("ACCESS EXCEPTION!");
                }
            }
            result.append(newLine);
        }
        result.append("}\n");
        return result;
    }

    /**
	 * Decreases the LogLevel
	 * FINEST (lowest value)
	 * FINER
	 * FINE
	 * CONFIG
	 * INFO
	 * WARNING
	 * SEVERE (highest value)
	 * and returns it.
	 * @return newLevel Level of logging,  Level.WARNING as lowest
	 */
	public Level decreaseLogLevel() {
		int oldLevelInt = log.getLevel().intValue();

		if (oldLevelInt == Level.WARNING.intValue()) {

			// Nothing to do
			return Level.WARNING;
		}

		int newLevelInt;

		if (oldLevelInt == Level.ALL.intValue()) {
			newLevelInt = Level.FINEST.intValue();
		} else {
			newLevelInt = oldLevelInt + oneHundred;
		}

		Level newLevel = Level.parse(Integer.toString(newLevelInt));

		log.setLevel(newLevel);

		return newLevel;
	}

	/**
	 * Increases the LogLevel
	 * SEVERE (highest value)
	 * WARNING
	 * INFO
	 * CONFIG
	 * FINE
	 * FINER
	 * FINEST (lowest value)
	 * and returns it.
	 * @return newLevel Level of logging,  Level.ALL as Highest
	 */
	public Level increaseLogLevel() {
		int oldLevelInt = log.getLevel().intValue();

		if (oldLevelInt == Level.ALL.intValue()) {

			// Nothing to do
			return Level.ALL;
		}

		int newLevelInt;

		if (oldLevelInt == Level.FINEST.intValue()) {
			newLevelInt = Level.ALL.intValue();
		} else {
			newLevelInt = oldLevelInt - oneHundred;
		}

		Level newLevel = Level.parse(Integer.toString(newLevelInt));

		log.setLevel(newLevel);

		return newLevel;
	}

	/**
	 * Returns the Level of logging
	 * @return Level   = FINEST, FINER, FINE, CONFIG, INFO, WARNING, SEVERE
	 */
	public Level returnLogLevel() {
		return log.getLevel();
	}

	/**
	 * formats the message by adding the [PluginName] in front.
	 * @param message to format, e.g. this is a test
	 * @return formated message, e.g. [PluginName] this is a test
	 */
	private String formatMessage(String message) {
		// leaving this in as perhaps implemented again in the future
		/* if (enableMessageFormatting) {
					return "[" + pluginName + "] " + message;
				}*/

		return message;
	}

	/**
	 * Enables logging to file and disables console logging if configured via {@see #logToConsoleAndFile}
	 * @param enableFileLog true / false
	 */
	private void configureFileLog(boolean enableFileLog) {
		this.enableFileLog = enableFileLog;

		if (enableFileLog) {
			try {
				File folder = new File(logDirectory);

				if (folder != null) {
					folder.mkdirs();
				}

				debug("Enabling File logging");
				debug("File will be: " + logFileName);
				fileHandler = new FileHandler(logDirectory + logFileName, true);
				log.setUseParentHandlers(logToConsoleAndFile);

				for (Handler handler : log.getHandlers()) {
					log.removeHandler(handler);
				}

				log.addHandler(fileHandler);
				fileHandler.setLevel(Level.ALL);
				fileHandler.setFormatter(new LogFormatter());
				wasMessageFormattingEnabled = enableMessageFormatting;

				if (!logToConsoleAndFile) {
					enableMessageFormatting = false;
				}
			} catch (SecurityException e) {
				severe("We have some issues switching to file logging", e);
				configureFileLog(false);
			} catch (IOException e) {
				severe("We have access issues to the file", e);
				configureFileLog(false);
			}
		} else {
			debug("Disabling File logging");
			log.setUseParentHandlers(true);

			for (Handler handler : log.getHandlers()) {
				log.removeHandler(handler);
			}
			if (fileHandler != null) {
				fileHandler.close();
			}

			enableMessageFormatting = wasMessageFormattingEnabled;
			debug("Console logging enabled");
		}
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Set the level of logging directly:
	 * SEVERE (highest value)
	 * WARNING
	 * INFO
	 * CONFIG
	 * FINE
	 * FINER
	 * FINEST (lowest value)
	 * @param level = FINEST, FINER, FINE, CONFIG, INFO, WARNING, SEVERE
	 */
	public void setLevel(Level level) {
		log.setLevel(level);
	}

	//~--- methods ------------------------------------------------------------

	/**
	 * will output [PluginName] v "VersionNumber" enabled
	 * or [PluginName] enabled
	 */
	public  void enableMsg() {
		if (pluginVersion.length() == 0) {
			info("enabled");
		} else {
			info("v " + pluginVersion + " enabled");
		}
	}

	/**
	 * will output [PluginName] v "VersionNumber" disabled
	 * or [PluginName] disabled
	 */
	public void disableMsg() {
		if (pluginVersion.length() == 0) {
			info("disabled");
		} else {
			info("v " + pluginVersion + " disabled");
		}
	}

	//  Getters & Setters

	/**
	 * Enable logging with level DEBUG to console or file
	 */
	public void enableDebug() {
		debugLogEnabled = true;
	}

	/**
	 * Disable logging with level DEBUG to console or file
	 */
	public void disableDebug() {
		debugLogEnabled = false;
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Check if we are logging to Console AND File at the same time
	 * @return logToConsoleAndFile
	 */
	public  boolean isLogToConsoleAndFile() {
		return logToConsoleAndFile;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Set if we are logging to Console and File at the same time
	 * @param logToConsoleAndFile true / false
	 */
	public void setLogToConsoleAndFile(boolean logToConsoleAndFile) {
		this.logToConsoleAndFile = logToConsoleAndFile;
	}

	//~--- methods ------------------------------------------------------------

	/**
	 * Enable Logging to Console and File at the same time
	 */
	public void enableLogToConsoleAndFile() {
		logToConsoleAndFile = true;
	}

	/**
	 * Disable Logging to Console and File at the same time
	 */
	public void disableLogToConsoleAndFile() {
		logToConsoleAndFile = false;
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Returns the pluginName being used by the logger
	 * @return pluginName
	 */
	public String getPluginName() {
		return pluginName;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Sets the pluginName being used by the logger to display messages. Use this if your class name isn't the name you
	 * want to be used during logging when you did use {@see #getLogger()}
	 * @param pluginName pluginName to be used to display messages
	 */
	public void setPluginName(String pluginName) {
		this.pluginName = pluginName;
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Returns the pluginVersion which is being used by the logger.
	 * @return pluginVersion
	 */
	public  String getPluginVersion() {
		return pluginVersion;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Sets the pluginVersion being used by the logger during enableMessage and disableMessage. Use this if you did use {@see #getLogger()}
	 * @param pluginVersion pluginVersion to be used for enableMessage and disableMessage
	 */
	public  void setPluginVersion(String pluginVersion) {
		this.pluginVersion = pluginVersion;
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Returns the pluginDirectory
	 * @return pluginDirectory
	 */
	public String getPluginDirectory() {
		return pluginDirectory;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Sets the pluginDirectory being used by the logger to save the log file to. Use this if your class name isn't the correct name you
	 * need to be used during logging when you did use {@see #getLogger()}
	 * @param pluginDirectory directory being used to write the log files
	 */
	public  void setPluginDirectory(String pluginDirectory) {
		this.pluginDirectory = pluginDirectory;
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Returns if DEBUG logging is enabled
	 * @return debugLogEnabled
	 */
	public boolean isDebugLogEnabled() {
		return debugLogEnabled;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Configures logging with DEBUG level.
	 * @param debugLogEnabled true / false
	 */
	public void setDebugLogEnabled(boolean debugLogEnabled) {
		this.debugLogEnabled = debugLogEnabled;
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Returns if File logging is enabled
	 * @return enableFileLog
	 */
	public boolean isEnableFileLog() {
		return enableFileLog;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Enables or disables the logging to file
	 * @param enableFileLog
	 */
	public void setEnableFileLog(boolean enableFileLog) {
		this.enableFileLog = enableFileLog;
		configureFileLog(enableFileLog);
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Returns the logFileName
	 * @return logFileName
	 */
	public String getLogFileName() {
		return logFileName;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Sets the logFileName being used by the logger to log messages to file. By default this is pluginName.log.
	 * Use this if your logFileName isn't the filename you want to be used during logging when you did use {@see #getLogger()}
	 * or if you want to name the file differently
	 * @param logFileName
	 */
	public void setLogFileName(String logFileName) {
		this.logFileName = logFileName;
	}

	//~--- methods ------------------------------------------------------------

	/**
	 * Enables fancy Exception logging
	 */
	public void enableFancyExceptionLog() {
		fancyExceptionLog = true;
	}

	/**
	 * Disable fancy Exception logging
	 */
	public void disableFancyExceptionLog() {
		fancyExceptionLog = false;
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Status of FancyExceptionLogging
	 * @return fancyExceptionLog
	 */
	public boolean isFancyExceptionLog() {
		return fancyExceptionLog;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Set fancyExceptionLog
	 * @param enable true / false of fancyExceptionLog
	 */
	public void setFancyExceptionLog(boolean enable) {
		fancyExceptionLog = enable;
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Returns the log directoy
	 * @return String LogDirectory
	 */
	public String getLogDirectory() {
		return logDirectory;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Sets the logDirectory being used by the logger to save the log file to. Use this if your class name isn't the correct name you
	 * need to be used during logging when you did use {@see #getLogger()}
	 * @param logDirectory
	 */
	public void setLogDirectory(String logDirectory) {
		this.logDirectory = logDirectory;
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Returns if we enabled Message formatting in the style of [PluginName] msg
	 * Note: there is no message formatting being done in the log file.
	 * @return enableMessageFormatting
	 */
	public boolean isEnableMessageFormatting() {
		return enableMessageFormatting;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Enables / Disables message formatting
	 * Note: there is no message formatting being done in the log file.
	 * @param enableMessageFormatting
	 */
	public void setEnableMessageFormatting(boolean enableMessageFormatting) {
		this.enableMessageFormatting = enableMessageFormatting;
	}

	//~--- get methods --------------------------------------------------------

	/**
	 * Returns the date format being used for logging to file, default is:
	 * "yyyy-MM-dd HH:mm:ss"
	 * @return dateFormat
	 */
	public String getDateFormat() {
		return dateFormat;
	}

	//~--- set methods --------------------------------------------------------

	/**
	 * Sets the date format being used for logging to file
	 * @param dateFormat
	 */
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}


	/**
	 * will output with specified level to console or file
	 * @param level LoggerLevel = FINEST, FINER, FINE, CONFIG, INFO, WARNING, SEVERE
	 * @param message message to output
	 */
	public void log(Level level, String message) {
		log.log(level, formatMessage(message));
	}

	//~--- inner classes ------------------------------------------------------

	/**
	 * LogFormatter Private class.
	 */
	private class LogFormatter extends Formatter {
		/**
		 * Dateformat being used for logging to file
		 */
		private final SimpleDateFormat date = new SimpleDateFormat(dateFormat);

		//~--- methods --------------------------------------------------------

		/**
		 * Formatter to generate the log file messages
		 * @param record
		 * @return
		 */
		@Override
		public String format(LogRecord record) {
			StringBuilder builder = new StringBuilder();
			@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
			Throwable ex = record.getThrown();

			builder.append(this.date.format(record.getMillis()));
			builder.append(" [");
			builder.append(record.getLevel().getLocalizedName().toUpperCase());
			builder.append("] ");
			builder.append(record.getMessage());
			builder.append('\n');

			if (ex != null) {
				StringWriter writer = new StringWriter();

				ex.printStackTrace(new PrintWriter(writer));
				builder.append(writer);
			}

			return builder.toString();
		}
	}


}
