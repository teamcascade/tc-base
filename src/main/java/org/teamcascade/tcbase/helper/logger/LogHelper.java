/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.helper.logger;

import java.util.logging.Level;

import org.teamcascade.tcbase.TC_Base;

import org.spout.api.Spout;

/**
 * A Helper which allows to log messages with the plugin name without passing meta data.
 * If Debug logging for TC_Base is enabled it will also log debug messages.
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class LogHelper {

    private static final java.util.logging.Logger LOG_HELPER_LOGGER = Spout.getLogger();

    public static void info (String msg){
        String logMsg = getCallingPlugin() + msg;
        LOG_HELPER_LOGGER.log(Level.INFO, logMsg);
    }

    public static void debug (String msg, Object o){
        if (!TC_Base.isDebugLogging()){
            return;
        }
        String logMsg = getCallingPlugin() + "[DEBUG] " + msg;
        if (o != null){
            logMsg = logMsg + " [" + o.toString() + "]";
        }
        LOG_HELPER_LOGGER.log(Level.INFO, logMsg);
    }

    public static void severe (String msg, Throwable ex){
        String plugin = getCallingPlugin();
        LOG_HELPER_LOGGER.log(Level.SEVERE, plugin + msg, ex);
    }

    public static void severe (String msg){
        severe(msg, null);
    }

    public static void debug (String msg){
        debug(msg,null);
    }

    private static String getCallingPlugin(){
        StackTraceElement[] ste = new Throwable().getStackTrace();
        String plugin = "TC_Base";
        for (StackTraceElement stack : ste){
            String pack = stack.getClassName().substring(stack.getClassName().indexOf('.',4));
            if (!pack.contains("tcbase") && !pack.contains("spout")){
                plugin = stack.getClassName().substring(stack.getClassName().lastIndexOf('.')+ 1);
                break;
            }
        }
        return "[" + plugin + "] ";
    }
}
