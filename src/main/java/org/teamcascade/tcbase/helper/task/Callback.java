/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.helper.task;

/**
 * Implemented anonymously by callers of the {@link TaskExecutor#executeAsyncTask(Task, Callback)} method to provide a
 * delegate that is invoked upon the completion of the asynchronous task. The callback is marshaled onto the foreground
 * thread before executing.
 *
 * @author Joe Clark
 * @see {@link TaskExecutor}
 * @since 1.0
 */
public interface Callback<T>
{
    /**
     * Invokes the callback. The input parameter is the value returned from the corresponding {@link Task}.
     *
     * @param input The result returned from the asynchronous task.
     */
    void execute(T input);
}