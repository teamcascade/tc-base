/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.helper.task;

import org.spout.api.Spout;
import org.spout.api.plugin.CommonPlugin;
import org.spout.api.scheduler.Scheduler;
import org.teamcascade.tcbase.objects.TC_Meta;

/**
 * <p/>
 * Defines a helpful component that is able to perform tasks on a background thread and then marshall a callback back
 * on to the Spout main thread for performing an action such as informing a player of the result.
 * <p/>
 * As a game built around a high-performance 3D rendering engine, care must be taken to avoid doing any processing on
 * Spout's main UI thread that may block or spin-lock for long periods of time. This would cause the game to "hang"
 * and appear unresponsive to the user. However, attempting to interact with or manipulate the Spout environment
 * from outside the main thread creates a major problem for concurrency issues.
 * <p/>
 * Thus, the best approach is to offload as much work as possible to a background thread, reserving the main thread for
 * only work which simply _must_ be done in the foreground (such as updating the state of blocks in the world or generally
 * interacting with the world in any non read-only way).
 * <p/>
 * The Spout API provides a {@link org.spout.engine.scheduler.SpoutScheduler} component for performing this kind of work, but it is cumbersome to
 * write code that runs asynchronously on a background thread and then marshal synchronously back on to the main thread.
 *
 * This class makes it much easier:
 * <blockquote><pre>
 *
 *   private final TaskExecutor taskExecutor; // get this from the {@link org.teamcascade.tcbase.TC_Base}
 *   public void doSomeWork(Player aLoggedInPlayer)
 *   {
 *       taskExecutor.executeAsyncTask(new Task&lt;String&gt;()
 *       {
 *           // This code is executed asynchronously on a background thread.
 *           &#64Override
 *           public String execute()
 *           {
 *                // Do some intensive calculation, interaction with a remote process or some other resource-intensive
 *                // or IO-bound operation.
 *                return "Hello World";
 *           }
 *       }, new Callback&lt;String&gt;()
 *       {
 *           // This code is executed synchronously on the main thread. The value of the input parameter is the value
 *           // returned from the execute method above.
 *           &#64Override
 *           public void execute(String input)
 *           {
 *              // double-check that the user is still logged in now, since some time may have elapsed while waiting
 *              // for the asynchronous task to complete.
 *              aLoggedInPlayer.chat(input);
 *           }
 *       });
 *   }
 * </pre> </blockquote>
 *
 * @author Joe Clark, $Author: dredhorse$
 * @since 1.0
 * @see Callback
 * @see Task
 * @version $FullVersion$
 */
public final class TaskExecutor
{
    private final CommonPlugin plugin;
    private final Scheduler scheduler;

    /**
     * Constructs the TaskExecutor.
     *
     * @param meta    The meta plugin object.
     */
    public TaskExecutor(final TC_Meta meta)
    {
        scheduler= Spout.getEngine().getScheduler();
        this.plugin = meta.getPlugin();
    }

    public <T> void executeAsyncTask(final Task<T> task)
    {
        scheduler.scheduleAsyncTask(plugin, new Runnable() {
            @Override
            public void run() {
                task.execute();
            }
        });
    }

    /**
     * Provides a convenient method for executing an asynchronous task on a background thread with a callback that is
     * automatically marshalled back on to the main thread when the async task is complete.
     *
     * @param task     The code to be executed in the background.
     * @param callback The callback to be executed in the foreground.
     * @param <T>      The type parameter specifies the type of the value returned from the {@link Task#execute()} method,
     *                 which is passed into the {@link Callback#execute(Object)} method as the input parameter.
     */
    public <T> void executeAsyncTask(final Task<T> task, final Callback<T> callback)
    {
        scheduler.scheduleAsyncTask(plugin, new Runnable() {
            @Override
            public void run() {
                final T result = task.execute();
                scheduler.scheduleSyncDelayedTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        callback.execute(result);
                    }
                });
            }
        });
    }
}