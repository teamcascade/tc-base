/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.helper.file;



/*
 * Copyright (c) 2004 David Flanagan.  All rights reserved.
 * This code is from the book Java Examples in a Nutshell, 3nd Edition.
 * It is provided AS-IS, WITHOUT ANY WARRANTY either expressed or implied.
 * You may study, use, and modify it for any non-commercial purpose,
 * including teaching and use in open-source projects.
 * You may distribute it non-commercially as long as you retain this notice.
 * For a commercial use license, or to purchase the book,
 * please visit http://www.davidflanagan.com/javaexamples3.
 */

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;

import org.teamcascade.tcbase.helper.logger.LogHelper;

/**
 * FileCopy2.java: this program copies the file named in its first argument to
 * the file named in its second argument, or to standard output if there is no
 * second argument.
 */
public class FileCopy {
	public static void main(String[] args) {
		FileInputStream fin = null; // Streams to the two files.
		FileOutputStream fout = null; // These are closed in the finally block.
		try {
			// Open a stream to for the input file and get a channel from it
			fin = new FileInputStream(args[0]);
			FileChannel in = fin.getChannel();

			// Now get the output channel
			WritableByteChannel out;
			if (args.length > 1) { // If there is a second filename
				fout = new FileOutputStream(args[1]); // open file stream
				out = fout.getChannel(); // get its channel
			} else { // There is no destination filename
				out = Channels.newChannel(System.out); // wrap stdout stream
			}

			// Query the size of the input file
			long numbytes = in.size();

			// Bulk-transfer all bytes from one channel to the other.
			// This is a special feature of FileChannel channels.
			// See also FileChannel.transferFrom()
			in.transferTo(0, numbytes, out);
		} catch (IOException e) {
			// IOExceptions usually have useful informative messages.
			// Display the message if anything goes wrong.
			LogHelper.severe("Something went wrong during filecopy",e);
		} finally {
			// Always close input and output streams. Doing this closes
			// the channels associated with them as well.
			try {
				if (fin != null)
					fin.close();
				if (fout != null)
					fout.close();
			} catch (IOException e) {
				LogHelper.severe("Something went wrong during filecopy",e);
			}
		}
	}
}
