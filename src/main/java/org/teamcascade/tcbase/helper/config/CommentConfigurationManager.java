/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.helper.config;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.teamcascade.tcbase.configurations.CONFIG;
import org.teamcascade.tcbase.exceptions.ConfigNotAvailableException;
import org.teamcascade.tcbase.exceptions.WrongClassException;
import org.teamcascade.tcbase.helper.Messenger;
import org.teamcascade.tcbase.helper.file.AutoFileCloser;
import org.teamcascade.tcbase.helper.file.UnicodeSaver;
import org.teamcascade.tcbase.interfaces.TC_Config_Enums;
import org.teamcascade.tcbase.objects.TC_Meta;

import org.spout.api.exception.ConfigurationException;
import org.spout.api.plugin.CommonPlugin;
import org.spout.api.plugin.Plugin;
import org.spout.api.plugin.PluginDescriptionFile;
import org.spout.api.util.config.yaml.YamlConfiguration;

/**
 * Handel's all the saving, loading, checking for new version, reloading and transferring the config to and from the enums.<br>
 * You shouldn't really change anything in here normally all.<br>
 * @param <T>
 * @author $Author: dredhorse$
 * @version $FullVersion$
 * @todo restructure more of this class, also implement passing of ENUM Class to handle saving of enums
 */
public class CommentConfigurationManager<T extends TC_Config_Enums> {

/* Line for logger */
	private static final String LINE = "=====================================";

	// ---- Default Values -----------------------------------------------------

	/**
	 * Name of the plugin
	 */
	private String pluginName;

	// Default plugin configuration
	/**
	 * Configuration File Name
	 */
	private String configFile = "config.yml";
	/**
	 * Do we require a config update?
	 */
	private boolean configRequiresUpdate = false;
	/**
	 * Has the config been changed via commands and isn't saved yet?
	 */
	private boolean configDirty = false;
	/**
	 * Do we have a non recoverable error?  Will be set if we have problems creating the default values
	 */
	private boolean nonRecoverableError = false;
	/**
	 * Did we try fixing yaml issues?
	 */
	private boolean triedFixingYamlIssues = false;
	/**
	 * Enable logging to separate file
	 */
	private boolean logToFile = true;

	// ======================= Default Configuration Stuff ==========================
	/**
	 * Enable more logging.. could be messy!
	 */
	private boolean debugLogEnabled = false;
	/**
	 * Enable logging of the config
	 */
	private boolean configLogEnabled = true;
	/**
	 * AutoUpdate the config file if necessary. This will overwrite any changes outside the configuration parameters!
	 */
	private boolean configAutoUpdate = true;
	/**
	 * Enable saving of the config file
	 */
	private boolean configAutoSave = true;
	/**
	 * Check if there is a new version of the plugin out.
	 */
	private boolean checkForUpdate = true;
	/**
	 * Object to handle the configuration
	 */
	private YamlConfiguration config;
	/**
	 * This is the internal config version, which is being used if no Version is supplied from the plugin itself
	 * use {@link #alterConfigCurrent(String conCurr)} for this
	 */
	private String configCurrent;
	/**
	 * This is the configuration file version read from the configuration file.
	 */
	private String configVer;
	/**
	 * Config Enums
	 */
	private TC_Config_Enums[] tcConfigEnums;
	/**
	 * PluginDescriptionFile
	 */
	private PluginDescriptionFile pdfFile;
	/**
	 * The plugin using this CommentConfigurationManager
	 */
	private CommonPlugin plugin;
	/**
	 * Directory of the plugin configuration
	 */
	private String pluginPath;
	/**
	 * Link to the location of the plugin website
	 * Note: This can be overwritten by {@link #alterPluginSlug(String plugSlug)}
	 */
	private String pluginSlug;
	/**
	 * Meta Information of the Plugin
	 */
	private TC_Meta meta;

	/**
	 * Constructor for CommentConfigurationManager, you will still need perhaps to configure some stuff.
	 * @param meta information of the plugin
	 * @param configEnum
	 * @see #initializeConfig() for more information
	 */
	public CommentConfigurationManager(TC_Meta meta, Class<T> configEnum) throws ConfigNotAvailableException {
		this.meta = meta;
		pdfFile = meta.getPlugin().getDescription();
		pluginName = pdfFile.getName();
		pluginPath = meta.getPlugin().getDataFolder() + System.getProperty("file.separator");
		plugin = meta.getPlugin();
		tcConfigEnums = configEnum.getEnumConstants();
		configFile = configEnum.getSimpleName() + ".yml";
		pluginSlug = meta.getTCManifest().getPluginURL();
		configCurrent = getConfigEnum("configversion").getString();
		initializeConfig();
	}

	/**
	 * Returns the requested config_enum, for example {@code configversion} will return the CONFIG_VERSION enum.
	 * The retrieval will be done with ignorecase.
	 * @param configEnum to look for
	 * @return enum or NULL if no enum was found
	 */
	public TC_Config_Enums getConfigEnum(String configEnum) {
		for (TC_Config_Enums ce : tcConfigEnums) {
			if (ce.toString().equalsIgnoreCase(configEnum)) {
				return ce;
			}
		}
		return null;
	}

	/**
	 * Allows changing of the Configuration Version, which is being used to compare against the version inside the configuration file.
	 * This will also alter the configVer value which is used to create the configuration file
	 * @param conCurr new String value of the new version
	 */
	public void alterConfigCurrent(String conCurr) {
		configCurrent = conCurr;
		configVer = conCurr;
	}

	/**
	 * Allows changing of the URL which is included in the configuration file
	 * @param plugSlug new String value of the URL
	 */
	public void alterPluginSlug(String plugSlug) {
		pluginSlug = plugSlug;
	}

	/**
	 * Method to check if the config file already exists
	 * @param configFile which is being used by the plugin
	 * @return true if configFile exists, false if not
	 */
	private boolean configFileExist(String configFile) {
		return (new File(((Plugin) plugin).getDataFolder(), configFile)).exists();
	}

	//  ======================= Reading the Configuration ===================

	/**
	 * Reading the configuration in via standard spout means, a WrongClassException will be handled and default used in
	 * this case.
	 * <p/>
	 * Please contact me with any issues here so that I can try to fix them in the {@link #configurationWriting(java.io.File)}  section
	 * or create a pull request for the changes.
	 */
	private void configurationReading() {
		meta.getLog().config(LINE);
		meta.getLog().config("Reading the configuration for file: " + configFile);

		for (TC_Config_Enums tcConfigEnum : tcConfigEnums) {
			if (tcConfigEnum.getConfigOption() != null) {
				try {
					tcConfigEnum.setConfigOption((config.getNode("configuration."
							+ tcConfigEnum.toString()).getValue() != null)
							? config.getNode("configuration."
							+ tcConfigEnum.toString()).getValue()
							: tcConfigEnum.getConfigOption());
				} catch (WrongClassException e) {

					// if we got a WrongClassException we try to get the right kind of class
					// todo make sure that we handle all cases correctly, please let me know
					meta.getLog().debug("Exception message", e);
					meta.getLog().debug("Getting the correct Class now!");

					Map<String, Object> map = config.getNode("configuration."
							+ tcConfigEnum.toString()).getValues();

					tcConfigEnum.setConfigurationOption(map);
				} finally {
					meta.getLog().debug(
							tcConfigEnum + ": ",
							tcConfigEnum.getConfigOption().toString().trim());
				}
			}
		}

		meta.getLog().config("Reading the configuration = done!");
	}

	// ======================= Writing the information to the file ===================

	/**
	 * Finally writing the configuration to the configuration file
	 * @param stream access to the config file
	 */
	private void configurationWriting(File stream) throws IOException {
		meta.getLog().config("Writing the configuration for file: " + configFile);
		UnicodeSaver.saveUTF8File(stream, "\n", true);
		UnicodeSaver.saveUTF8File(stream, "configuration: \n", true);
		UnicodeSaver.saveUTF8File(stream, "\n", true);

		for (TC_Config_Enums tcConfigEnum : tcConfigEnums) {
			Object configOption = tcConfigEnum.getConfigOption();

			UnicodeSaver.saveUTF8File(stream, "#   " + tcConfigEnum.getConfigComment() + "\n",
					true);

			// let's check if we just had a comment or also an option
			if (configOption != null) {
				meta.getLog().debug("Config Value: " + tcConfigEnum.toString(), configOption);

				String configOptionLine = "    " + tcConfigEnum + ": ";

				// check if it is a int, boolean, char, byte, short, double, long, float,
				// in which case we don't have to do anything when writing.
				if (isPrimitiveWrapper(configOption)) {
					configOptionLine = configOptionLine + configOption.toString();
				} else {

					// looks like we do have something different than the primitive wrapper can handle
					// so let's check if we are a map
					if (configOption instanceof Map) {
						UnicodeSaver.saveUTF8File(stream, configOptionLine + "\n", true);

						Iterator it = ((Map) configOption).keySet().iterator();
						Object nextIt;

						while (it.hasNext()) {
							nextIt = it.next();

							if (it instanceof Map) {
								UnicodeSaver.saveUTF8File(stream,
										"        " + nextIt.toString().trim()
												+ ": \n", true);

								Iterator innerIt = ((Map) configOption).keySet().iterator();
								Object nextInnerIt;

								while (innerIt.hasNext()) {
									nextInnerIt = innerIt.next();

									Object mapOption = ((Map) nextIt).get(nextInnerIt);

									if (mapOption instanceof String) {
										UnicodeSaver.saveUTF8File(stream,
												"            "
														+ nextInnerIt.toString().trim()
														+ ": \""
														+ mapOption.toString().trim()
														+ "\" \n", true);
									} else {
										UnicodeSaver.saveUTF8File(stream,
												"            "
														+ nextInnerIt.toString().trim()
														+ ": "
														+ mapOption.toString().trim()
														+ "\n", true);
									}
								}
							} else {
								Object mapOption = ((Map) configOption).get(nextIt);

								if (mapOption instanceof String) {
									UnicodeSaver.saveUTF8File(stream,
											"        " + nextIt.toString().trim()
													+ ": \"" + mapOption.toString().trim()
													+ "\" \n", true);
								} else {
									UnicodeSaver.saveUTF8File(stream,
											"        " + nextIt.toString().trim()
													+ ": " + mapOption.toString().trim()
													+ "\n", true);
								}
							}
						}

						configOptionLine = "";
					} else {

						// or if we are a list
						if (configOption instanceof List) {
							UnicodeSaver.saveUTF8File(stream, configOptionLine + "\n", true);

							for (String listItem : (List<String>) (List<?>) configOption) {
								UnicodeSaver.saveUTF8File(stream, "        - \"" + listItem + "\" \n", true);
							}

							configOptionLine = "";
						} else {

							// if it is a String let's wrap it in "
							if (configOption instanceof String) {
								configOptionLine = configOptionLine + "\"" + configOption + "\"";
							} else {

								// well let's just dump it in any other case
								// ToDo make sure that this works or create other approaches for certain cases
								configOptionLine = configOptionLine
										+ configOption.toString().trim();
							}
						}
					}
				}

				UnicodeSaver.saveUTF8File(stream, configOptionLine + "\n", true);
				UnicodeSaver.saveUTF8File(stream, "\n", true);
			}
		}
	}

	/**
	 * Method which will use the DEBUG_LOG_ENABLED, LOG_TO_FILE, CHECK_FOR_UPDATE, CONFIG_AUTO_UPDATE, CONFIG_AUTO_SAVE, CONFIG_LOG_ENABLED
	 * enums to overwrite the default values configured in here.
	 * <p/>
	 * If you don't want to use those enums make sure that you delete the corresponding references here!
	 */
	private void defaultInit() {
		debugLogEnabled = getConfigEnum("debugLogEnabled").getBoolean();
		meta.getLog().debug("debugLogEnabled [" + debugLogEnabled + "]");
		logToFile = getConfigEnum("logToFile").getBoolean();
		checkForUpdate = getConfigEnum("CheckForUpdate").getBoolean();
		configAutoUpdate = getConfigEnum("ConfigAutoUpdate").getBoolean();
		configAutoSave = getConfigEnum("ConfigAutoSave").getBoolean();
		configLogEnabled = getConfigEnum("ConfigLogEnabled").getBoolean();
		meta.getLog().setLevel(Level.parse(getConfigEnum("LogLevel").getString()));
		meta.getLog().config("ConfigLogEnabled [" + configLogEnabled + "]");
		meta.getLog().config("LogLevel [" + meta.getLog().returnLogLevel() + "]");
	}

	/**
	 * Fixes the most common issues in yaml files:
	 * <ul>
	 * <li>tabs instead spaces
	 * <li>keys not having a : at the end
	 * <li>strings not being escaped by ' or " correctly
	 * </ul>
	 */
	private void fixYamlIssues() {

		// todo check if saving in utf-8 is needed
		triedFixingYamlIssues = true;

		meta.getLog().config("Trying to fix the yaml file from issues!");

		/**
		 * Code taken from http://bit.ly/Oiz1dG
		 * bPermissions / src / de / bananaco / bpermissions / imp / YamlFile.java
		 * fixed some issues
		 */
		final List<String> data = new ArrayList<String>();

		try {
			new AutoFileCloser() {
				@Override
				protected void doWork() throws Throwable {
					BufferedWriter bw = autoClose(bw = new BufferedWriter(new FileWriter(pluginPath + "temp.yml")));
					BufferedReader br = autoClose(br = new BufferedReader(new FileReader(pluginPath + configFile)));

					String line;

					while ((line = br.readLine()) != null) {
						data.add(line);
					}
					List<String> newData = new ArrayList<String>();
					if (data.size() == 0) {
						nonRecoverableError = true;
   						meta.getLog().severe(
								"Some files where not found during yaml fixing... please check your storage!");
						return;
					}

					for (int i = 0; i < data.size(); i++) {
						line = data.get(i);

						// Replace tabs with 4 spaces
						if (line.contains("\t")) {
							meta.getLog().debug("line " + i + " of " + configFile
									+ " contained a tab. A fix was attempted.");

							while (line.contains("\t")) {
								line = line.replace("\t", "    ");
							}
						}

						// Make sure keys have a key: at the end
						if (!line.replaceAll(" ", "").endsWith(":")
								&& !line.replaceAll(" ", "").startsWith("-") && !line.endsWith("[]")
								&& !line.contains(": ")) {
							meta.getLog().debug("line " + i + " of " + configFile
									+ " contained a missing : . A fix was attempted.");

							line = line + ":";
						}

						// Make sure that all 'strings' in a - 'string' list are escaped
						if (line.replaceAll(" ", "").startsWith("-")
								&& line.replaceAll(" ", "").replace("-", "").startsWith("'")
								&& !line.endsWith("'")) {
							meta.getLog().debug(
									"line " + i + " of " + configFile
											+ " contained a non closed string with '. A fix was attempted.");

							line = line + "'";
						}

						// Make sure that all "strings" in a - "string" list are escaped
						if (line.replaceAll(" ", "").startsWith("-")
								&& line.replaceAll(" ", "").replace("-", "").startsWith("\"")
								&& !line.endsWith("\"")) {
							meta.getLog().debug(
									"line " + i + " of " + configFile
											+ " contained a non closed string with \". A fix was attempted.");

							line = line + "\"";
						}

						// Ignore blank lines
						if (line.replaceAll(" ", "").equals("")) {
							meta.getLog().debug("line " + i + " of " + configFile
									+ " contained a blank line. A fix was attempted.");

							line = line.replaceAll(" ", "");
						}

						if (line.equals(":")) {
							meta.getLog().debug("line " + i + " of " + configFile
									+ " contained a just a : . A fix was attempted.");

							line = line.replaceAll(":", "");
						} else {
							newData.add(line);
						}
					}

					for (String aNewData : newData) {
						bw.write(aNewData);
						bw.newLine();
					}
				}
			};


			// than delete the original and rename the temp file
			File oldConfigFile = oldConfigFile = new File(pluginPath + configFile);
			File tempFile = new File(pluginPath + "temp.yml");

			oldConfigFile.delete();
			tempFile.renameTo(oldConfigFile);
			meta.getLog().config("I tried my best, try reloading!");

			} catch (RuntimeException e) {
			meta.getLog().severe("Some files where not found during yaml fixing... please check your storage!", e);
			nonRecoverableError = true;
		}
	}

	/**
	 * Initialize the Config, this should done be AFTER setting the needed values,
	 * which can be either values needed for the ENUMs (see the example code) or
	 * values needed for the class itself:
	 * <p/>
	 * @throws ConfigNotAvailableException when the config can not correctly be initialized
	 * @see #alterConfigCurrent(String)
	 * @see #alterPluginSlug(String)
	 *      <p/>
	 *      we do the following:
	 *      <ul>
	 *      <li>create the config if it doesn't exist {@link #writeConfigToFile()}
	 *      <li>load the config to memory {@link #loadConfigToMemory(String)}
	 *      <li>fix the most common yaml issues if necessary {@link #fixYamlIssues()}
	 *      <li>check if we could read something at all
	 *      <li>apply the config to the enums {@link #configurationReading()}
	 *      <li>set debug and config level correctly
	 *      <li>check if the configuration requires an update and saves it if configured {@link #reloadConfig()}
	 *      </ul>
	 */
	private void initializeConfig() throws ConfigNotAvailableException {

		// make sure that we use the correct configFileVer
		configVer = configCurrent;

		// checking  if the configuration file exists, otherwise create it via the defaults
		if (!configFileExist(configFile) && !writeConfigToFile()) {
			meta.getLog().config("Using internal defaults");
		}

		/**
		 *  load the configuration into memory, if there are issues try to fix them.
		 *  the hopefully fixed configuration is auto loaded again, if it fails again
		 *  we throw a ConfigNotAvailableException
		 */

		if (!loadConfigToMemory(configFile)) {
			fixYamlIssues();

			if (!loadConfigToMemory(configFile) && nonRecoverableError) {
				throw new ConfigNotAvailableException(meta);
			}
		}

		// limited check if something was written at all into the configuration file
		if (config.getNode("configuration.ConfigVersion").getString() == null) {
			meta.getLog().severe("Config is either missing or has wrong syntax!");

			throw new ConfigNotAvailableException(meta);
		}

		// getting the version of the configuration file
		configVer = config.getNode("configuration.ConfigVersion").getString();

		// parsing the config back into the enums
		meta.getLog().config(LINE);
		meta.getLog().config("Applying the configuration");
		configurationReading();
		meta.getLog().config("Applying the configuration = done!");
		meta.getLog().config(LINE);

		// overwriting internal default if necessary
		defaultInit();

		// changing debug level if necessary
		meta.getLog().setDebugLogEnabled(debugLogEnabled);

		// changing config logging if necessary
		if (configLogEnabled) {
			meta.getLog().setLevel(Level.CONFIG);
		}

		isConfigRequiresUpdate();

		// let's update the configuration if enabled
		if (configAutoUpdate && configRequiresUpdate) {
			meta.getLog().config("Auto updating the configuration file");
			reloadConfig();
		}
	}

	/**
	 * Loads the configuration into memory via {@link #loadConfigToMemory(String)} and will than change the default config options<p>
	 * @return true if loading was successful
	 */
	public boolean loadConfig() {
		boolean success = false;

		meta.getLog().config(LINE);
		meta.getLog().config("Loading the configuration");

		try {
			loadConfigToMemory(configFile);

			success = true;

			meta.getLog().config("Configuration successfully loaded");
			defaultInit();
		} catch (ConfigNotAvailableException e) {
			meta.getLog().warning("There was a problem with loading the configuration", e);
		}

		return success;
	}

	/**
	 * Method to load the config file to memory
	 * @param configFile which is being used by the plugin
	 * @return true if retry it needed again after fixing yaml issues
	 * @throws ConfigNotAvailableException if we already had a problem with creating the default values
	 */
	private boolean loadConfigToMemory(String configFile) throws ConfigNotAvailableException {
		boolean success = false;

		config = new YamlConfiguration(new File(pluginPath + configFile));

		try {
			config.load();

			success = true;
		} catch (ConfigurationException e) {
			if (nonRecoverableError && triedFixingYamlIssues) {
				throw new ConfigNotAvailableException(meta, e);
			} else {
				meta.getLog().severe("Problem with the configuration in " + configFile
						+ "!", e);
			}
		}

		return success;
	}

	/**
	 * Method to handle the reloading of the configuration, this is done if configured automatically
	 * when the config was AutoUpdated, changed or manually.
	 * Note: This method will save the config if there was a change before, otherwise it will just load the config again.
	 * Note: This method doesn't need to be called explicitly, but you can if you want
	 * @return true if the reload was successful, false if there was an issue.
	 */
	public boolean reloadConfig() {
		boolean success = false;

		if (saveConfig()) {
			success = true;
		}

		if (success) {
			if (!loadConfig()) {
				success = false;
			} else {
				meta.getLog().config("Configuration successfully reloaded");
			}
		}

		meta.getLog().config(LINE);

		return success;
	}

	/**
	 * Method to save the configuration to file, this is done automatically if configured after every change.<br>
	 * This method will only save if the configuration was changed, if the config file doesn't exist it will try to create it.
	 * @return true if the save was successful or no change required
	 */
	public boolean saveConfig() {
		boolean success = true;

		if (!configFileExist(configFile)) {
			meta.getLog().debug("Config file doesn't exist, trying to create default");

			configDirty = true;
		}

		if (configDirty) {
			if (writeConfigToFile()) {
				configDirty = false;
			} else {
				success = false;
			}
		} else {
			meta.getLog().debug("Nothing to save, skipping.");
		}

		return success;
	}

	// ======================= Default Headers ======================================

	/**
	 * Default config file header.
	 * Note: Normally you don't want to change anything in here, if you really want to please feel free to do so,
	 * but don't mess it up!
	 * NOTE: Removing the configVer node will cause a ConfigNotAvailableException!
	 * @param stream access to the config file
	 */
	private void topHeader(File stream) throws IOException {
		UnicodeSaver.saveUTF8File(stream,
				"# " + pluginName + " " + pdfFile.getVersion() + " by "
						+ getAuthors((Plugin) plugin) + "\n", true);
		UnicodeSaver.saveUTF8File(stream, "#\n", true);
		UnicodeSaver.saveUTF8File(stream, "# Configuration File for " + pluginName + " " + configFile + ".\n", true);
		UnicodeSaver.saveUTF8File(stream, "#\n", true);
		UnicodeSaver.saveUTF8File(stream,
				"# For detailed assistance please visit: " + pluginSlug + "\n",
				true);
		UnicodeSaver.saveUTF8File(stream, "\n", true);
		UnicodeSaver.saveUTF8File(stream, "\n", true);
		UnicodeSaver.saveUTF8File(stream, "# ------- Plugin Configuration\n", true);
		UnicodeSaver.saveUTF8File(stream, "\n", true);
	}

	/**
	 * Method to handle the writing of the config file, either initially or afterwards
	 * @return true if writing was successful
	 */
	private boolean writeConfigToFile() {
		meta.getLog().config(LINE);
		meta.getLog().config("Saving the configuration file");

		boolean success = false;
		File configurationFile;

		try {

			final File folder = ((Plugin) plugin).getDataFolder();

			if (folder != null) {
				folder.mkdirs();
			}

			configurationFile = new File(pluginPath + configFile);

			UnicodeSaver.saveUTF8File(configurationFile, "", false);
			topHeader(configurationFile);
			configurationWriting(configurationFile);

			meta.getLog().config("Finished writing the configuration file");

			success = true;
		} catch (FileNotFoundException e) {
			meta.getLog().warning("Error saving the " + configFile + ".", e);
		} catch (IOException e) {
			meta.getLog().warning("Error saving the " + configFile + ".", e);
		}

		return success;
	}

	/**
	 * Convenience method to get the state of {@link CONFIG#CONFIG_AUTO_SAVE}
	 * @return state of {@link CONFIG#CONFIG_AUTO_SAVE}
	 */
	public boolean isConfigAutoSave() {
		return getConfigEnum("configautosave").getBoolean();
	}

	/**
	 * Checks if the config requires an update by comparing the configVer key from the config with the configCurrent field. <p>
	 * If the config file requires an update we flag the config dirty.
	 * </p>
	 * @return true if the config requires an update
	 */
	private boolean isConfigRequiresUpdate() {
		meta.getLog().config("Checking configuration version");

		if (configVer.equalsIgnoreCase(configCurrent)) {
			configRequiresUpdate = false;

			meta.getLog().config("Configuration is up to date!");

			return false;
		}

		configRequiresUpdate = true;
		configDirty = true;

		meta.getLog().config("Configuration is not up to date!");
		meta.getLog().config("Loaded version: [" + configVer + "], Current Version: ["
				+ configCurrent + "]");
		meta.getLog().config("You should update the configuration!");

		try {
			getConfigEnum("configversion").setConfigOption(configCurrent);
		} catch (WrongClassException e) {
			meta.getLog().debug("Well that shouldn't happen", e);
		}

		return true;
	}

	/**
	 * Code taken from MemorySection Class of Bukkit<p>
	 * checks if an object can be primitively wrapped with toString()
	 * </p>
	 * @param input object which is checked
	 * @return true if it can be primitively wrapped
	 */
	private boolean isPrimitiveWrapper(Object input) {
		return (input instanceof Integer) || (input instanceof Boolean)
				|| (input instanceof Character) || (input instanceof Byte)
				|| (input instanceof Short) || (input instanceof Double) || (input instanceof Long)
				|| (input instanceof Float);
	}

	/**
	 * The best way to set the configuration options, as this will flag the config dirty and will also trigger a save of the config<p>
	 * if {@link CONFIG#CONFIG_AUTO_SAVE} is enabled
	 * </p>
	 * @param tcConfigEnumsNode configuration node to set
	 * @param value object value to set
	 * @return false if there was a WrongClassException
	 */
	public boolean set(TC_Config_Enums tcConfigEnumsNode, Object value) {
		try {
			tcConfigEnumsNode.setConfigOption(value);
		} catch (WrongClassException e) {
			meta.getLog().severe(e.getMessage(), e);

			return false;
		}

		meta.getLog().fine(tcConfigEnumsNode + " was updated with: [" + value.toString()
				+ "]");

		configDirty = true;

		if (isConfigAutoSave()) {
			if (saveConfig()) {
				configDirty = false;

				meta.getLog().config("Configuration was successfully written");
			} else {
				meta.getLog().config("There was a problem saving the configuration");
			}

			meta.getLog().config(LINE);
		}

		return true;
	}

	/**
	 * Parse the Authors List into a readable String with ',' and 'and'.
	 * taken from MultiVerse-core https://github.com/Multiverse/Multiverse-Core
	 * @return nicely parsed string
	 */
	private String getAuthors(Plugin plugin) {
		List<String> auths = plugin.getDescription().getAuthors();
		return Messenger.getListAsNiceString(auths);
	}
}
