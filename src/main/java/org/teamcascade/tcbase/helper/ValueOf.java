/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.helper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Taken from http://java.dzone.com/articles/enum-tricks-customized-valueof
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class ValueOf {

    /**
     * Private Constructor
     */
    private ValueOf(){ }

	// EnumClass -> FieldName -> FieldValue
	private static final Map<Class<? extends Enum<?>>, Map<String, Map<?, Enum<?>>>> MAP =
		new HashMap<Class<? extends Enum<?>>, Map<String, Map<?, Enum<?>>>>();

    private static String NO_ENUM_CONST = "No enum const ";

    public static <T extends Enum<T>> T valueOf(Class<T> enumType, Comparable<T> comparable) {
		for (T v : values(enumType)) {
			if (comparable.compareTo(v) == 0) {
				return v;
			}
		}
		throw new IllegalArgumentException(
				NO_ENUM_CONST + enumType + "." + comparable);
    }


    public static <T extends Enum<T>, V> T valueOf(Class<T> enumType, String fieldName, V value) {
    	Map<String, Map<?, Enum<?>>> enumMap = MAP.get(enumType);
    	synchronized(MAP) {
			if (enumMap == null) {
				enumMap = new LinkedHashMap<String, Map<?, Enum<?>>>();
				MAP.put(enumType, enumMap);
				T[] values = values(enumType);
				for (Field field : enumType.getDeclaredFields()) {
					Map<Object, Enum<?>> fieldMap = new LinkedHashMap<Object, Enum<?>>();
					enumMap.put(field.getName(), fieldMap);
					for (T v : values) {
						fieldMap.put(getFieldValue(enumType, v, fieldName), v);
					}
				}
			}
    	}

    	enumMap = MAP.get(enumType);
		if (enumMap == null) {
			throw new IllegalStateException("Enum " + enumType + " is not initialized");
		}
		Map<?, Enum<?>> fieldMap = enumMap.get(fieldName);
		if (fieldMap == null) {
			throw new IllegalArgumentException("Field " + fieldName + " is not defined in Enum " + enumType);
		}
		T v = getField(fieldMap, value);
		if (v == null) {
			throw new IllegalArgumentException(NO_ENUM_CONST + enumType);
		}

    	return v;
    }


    @SuppressWarnings("unchecked")
	private static <T, V> T getField(Map<?, Enum<?>> fieldMap, V value) {
    	return (T)fieldMap.get(value);
    }


    @SuppressWarnings("unchecked")
	public static <T extends Enum<T>> T[] values(Class<T> enumType) {
    	Throwable t = null;
    	try {
			Method values = enumType.getMethod("values");
			return (T[])values.invoke(null);
		} catch (SecurityException e) {
			t = e;
		} catch (NoSuchMethodException e) {
			t = e;
		} catch (IllegalArgumentException e) {
			t = e;
		} catch (IllegalAccessException e) {
			t = e;
		} catch (InvocationTargetException e) {
			t = e;
		}

		throw new IllegalArgumentException(NO_ENUM_CONST + enumType, t);
	}


    private static <T> Object getFieldValue(Class<?> enumType, T enumElem, String fieldName) {
    	Throwable t = null;
    	try {
    		Field field = enumType.getDeclaredField(fieldName);
    		field.setAccessible(true);
			return field.get(enumElem);
		} catch (SecurityException e) {
			t = e;
		} catch (IllegalArgumentException e) {
			t = e;
		} catch (IllegalAccessException e) {
			t = e;
		} catch (NoSuchFieldException e) {
			t = e;
		}

		throw new IllegalArgumentException(NO_ENUM_CONST + enumType, t);
    }
}
