/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.objects;


/**
 * Object which holds the information from a Jira Issue
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class JiraIssue {
	// Description of the Issue
	private String description;
	// Key of the Issue
	private String keyID;
	// Link to the Issue
	private String link;
	// Resolution of the Issue
	private String resolutionID;
	// Status of the Issue
	private String statusID;
	// Title of the Issue
	private String title;
	// Priority of the Issue
	private String priority;
	// Type of the Issue
	private String type;

	/**
	 * The Issue description
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * The Issue Key
	 * @return
	 */
	public String getKeyID() {
		return keyID;
	}

	/**
	 * The Issue Link
	 * @return
	 */
	public String getLink() {
		return link;
	}

	/**
	 * The Issue resolution status
	 * @return
	 */
	public String getResolutionID() {
		return resolutionID;
	}

	/**
	 * The Issue status
	 * @return
	 */
	public String  getStatusID() {
		return statusID;
	}

	/**
	 * The Issue Title
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the Issue Description
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Set the Issue ID
	 * @param keyID
	 */
	public void setKeyID(String keyID) {
		this.keyID = keyID;
	}

	/**
	 * Set the Issue Link
	 * @param link
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * Set the issue resolution status
	 * @param resolutionID
	 */
	public void setResolutionID(String resolutionID) {
		this.resolutionID = resolutionID;
	}

	/**
	 * Set the issue status
	 * @param statusID
	 */
	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}

	/**
	 * Set the issue title
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * The Issue priority
	 * @return
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * Set the issue priority
	 * @param priority
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * The Issue Type
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * Set the issue type
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
}

