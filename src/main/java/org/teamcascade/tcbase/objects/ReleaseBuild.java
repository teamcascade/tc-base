/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.objects;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Object which holds the information from a Release Build
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class ReleaseBuild {

	// The Title of the Build
	private String title;
	// The Link to the Build
	private String link;
	// The Description of the Build
	private String description;
	// The version of the Build
	private String version;

	/**
	 * Get the cleared title of the Build
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the title of the build, the title will be cleared of HTML and also the Version will be set
	 * @param title to set
	 */
	public void setTitle(String title) {
		this.title = title.replaceAll("\\<.*?\\>", "");
		parseVersion(title);
	}

	/**
	 * Get the link to the Build
	 * @return
	 */
	public String getLink() {
		return link;
	}

	/**
	 * Set the Link to the Build. Note: The path to the artifact will be added automatically
	 * @param link
	 */
	public void setLink(String link) {
		this.link = link + "/artifact";
	}

	/**
	 * Get the description of the Build
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set the description of the build, the description will be cleared of HTML
	 * @param description
	 */
	public void setDescription(String description) {
		String[] temp = description.replaceAll("\\<.*?\\>", "").replaceAll("&.t;", "").split("^\\r?\\n");
		StringBuilder stringBuilder = new StringBuilder();
		for (String t : temp){
			stringBuilder.append(t.replaceAll("(?m)^\\s+$","").replaceAll("^[ \t]+","").trim());
		}
		this.description = stringBuilder.toString();
		// todo fix outpout with spaces
	}

	/**
	 * Get the version of the build
	 * @return
	 */
	public String getVersion() {
		return version;
	}

	private void parseVersion(String text){
		Pattern versionPattern = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)?");
		Matcher m = versionPattern.matcher(text);
		if (!m.find()) {
		    throw new IllegalArgumentException("Version must be in form <major>.<minor>.<micro>");
		}
		int groups = m.groupCount();
		if (groups != 3) {
		     throw new IllegalArgumentException("Version must be in form <major>.<minor>.<micro>");
		}
		version = m.group(1) +"." + m.group(2) +"." +  m.group(3);
	}

}
