/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */



package org.teamcascade.tcbase.objects;

import org.teamcascade.tcbase.exceptions.ConfigNotAvailableException;
import org.teamcascade.tcbase.helper.config.CommentConfigurationManager;
import org.teamcascade.tcbase.helper.logger.Logger;
import org.teamcascade.tcbase.notifications.API_Check;
import org.teamcascade.tcbase.notifications.JiraIssue_Check;
import org.teamcascade.tcbase.notifications.ReleaseBuild_Check;

import org.spout.api.Spout;
import org.spout.api.plugin.CommonPlugin;

/**
 * Meta information of the plugin which is being passed into TC_Base
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class TC_Meta {

	/**
	 * The logger for this plugin.
	 */
	private Logger	log;

	/**
	 * The main config of the plugin
	 */
	private CommentConfigurationManager	config;

	/**
	 * The Jira Issue Checker for this plugin
	 */
	private JiraIssue_Check jiraIssueCheck;

	/**
	 * The plugin instance of this plugin
	 */
	private CommonPlugin	plugin;

	/**
	 * The ReleaseBuild Checker for this plugin
	 */
	private ReleaseBuild_Check releaseBuildCheck;

	/**
	 * The Manifest for this plugin
	 */
	private TC_Manifest tcManifest;

	/**
	 * The Config Class for this plugin
	 */
	private Class configClass;

	/**
	 * The API Checker for this plugin
	 */
	private API_Check apiCheck;


	/**
	 * Creates the Meta Information based on the plugin and the Manifest
	 *
	 * @param plugin
	 */
	public TC_Meta (CommonPlugin plugin, Class configClass) {
		this.plugin = plugin;
		this.configClass = configClass;
		log         = new Logger(plugin);
		finishInitialization();
	}

	private void finishInitialization () {
		tcManifest = new TC_Manifest(this);
		if (!tcManifest.isManifestLoaded()){
			log.severe("The plugin is broken internally, disabling it now");
			Spout.getPluginManager().disablePlugin(plugin);
		}
		try {
			config = new CommentConfigurationManager(this, configClass);
		} catch (ConfigNotAvailableException e) {
			log.severe("UUps...", e);
		}
		jiraIssueCheck = new JiraIssue_Check(this);
		releaseBuildCheck = new ReleaseBuild_Check(this);
		apiCheck = new API_Check(this);
	}

    /**
     * Get the Plugin specific logger
     * @return
     */
	public Logger getLog() {
		return log;
	}

    /**
     * Get the Plugin specific configuration
     * @return
     */
	public CommentConfigurationManager getConfig() {
		return config;
	}

    /**
     * Get the Plugin specific JiraIssueCheck instance
     * @return
     */
	public JiraIssue_Check getJiraIssueCheck() {
		return jiraIssueCheck;
	}

    /**
     * Get the plugin for this meta information
     * @return
     */
	public CommonPlugin getPlugin() {
		return plugin;
	}

    /**
     * Get the Plugin specific ReleaseBuildCheck instance
     * @return
     */
	public ReleaseBuild_Check getReleaseBuildCheck() {
		return releaseBuildCheck;
	}

    /**
     * Get the Plugin specific Manifest
     * @return
     */
	public TC_Manifest getTCManifest() {
		return tcManifest;
	}

    /**
     * Get the Plugin specific APICheck instance
     */
	public API_Check getApiCheck() {
		return apiCheck;
	}

}
