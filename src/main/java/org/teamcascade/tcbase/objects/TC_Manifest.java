/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.objects;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * Loads the manifest for the plugins.
 * Adapted from http://stackoverflow.com/questions/1272648/reading-my-own-jars-manifest
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class TC_Manifest {

	// Version of TC_Base which is required by this plugin
	private String tcBaseVersion;

	// Version of the Spout API which is required by this plugin
	private String spoutAPI;

	// Version of the Game API which is required by this plugin
	private String gameAPI;

	// The Plugin URL
	private String pluginURL;

	//The config for the Jira Issues notification
	private String	requestID;

	// The ID of the Release Build
	private String	releaseBuildID;

	// The location of git repo
	private String gitRepoUrl;

	// If we loaded the manifest
	private boolean manifestLoaded = false;

    // The path of the manifest
    private static final String ORG_TEAMCASCADE = "org/teamcascade/";

	public TC_Manifest(TC_Meta meta) {
		Class clazz = meta.getPlugin().getClass();
		String className = clazz.getSimpleName() + ".class";
		String classPath = clazz.getResource(className).toString();
		if (!classPath.startsWith("jar")) {
			meta.getLog().debug("Not correct", classPath);
			return;
		}
		String manifestPath = classPath.substring(0, classPath.lastIndexOf('!') + 1) +
				"/META-INF/MANIFEST.MF";
		Manifest manifest = null;
		try {
			manifest = new Manifest(new URL(manifestPath).openStream());
		} catch (IOException e) {
			meta.getLog().severe("There was an issue with the Manifest-File",e);
		}
		Map<String, Attributes> attr = null;
		if (manifest != null) {
			meta.getLog().debug("Manifest",manifest);
			manifestLoaded = true;
			attr = manifest.getEntries();
			meta.getLog().debug("TC_Base-API",attr.get(ORG_TEAMCASCADE).getValue("TC_Base-API"));
			tcBaseVersion = attr.get(ORG_TEAMCASCADE).getValue("TC_Base-API");
			spoutAPI = attr.get(ORG_TEAMCASCADE).getValue("Spout-API");
			gameAPI = attr.get(ORG_TEAMCASCADE).getValue("Game-API");
			pluginURL = attr.get(ORG_TEAMCASCADE).getValue("PluginURL");
			gitRepoUrl = attr.get(ORG_TEAMCASCADE).getValue("GitRepoUrl");
			requestID = attr.get(ORG_TEAMCASCADE).getValue("RequestID");
			releaseBuildID = attr.get(ORG_TEAMCASCADE).getValue("ReleaseBuildID");
		}
	}

	/**
	 * The Version of TC_Base which is required by this plugin
	 * @return
	 */
	public String getTcBaseVersion() {
		return tcBaseVersion;
	}

	/**
	 * The Version of the Spout API which is required by this plugin
	 * @return
	 */
	public String getSpoutAPI() {
		return spoutAPI;
	}

	/**
	 * The Version of the Game API which is required by this plugin
	 * @return
	 */
	public String getGameAPI() {
		return gameAPI;
	}

	/**
	 * Did we load the manifest
	 * @return
	 */
	public boolean isManifestLoaded() {
		return manifestLoaded;
	}

	/**
	 * The URL of the plugin
	 * @return
	 */
	public String getPluginURL() {
		return pluginURL;
	}

	/**
	 * The config for the Jira Issues notification
	 * @return
	 */
	public String getRequestID() {
		return requestID;
	}

	/**
	 * The ID of the Release Build
	 * @return
	 */
	public String getReleaseBuildID() {
		return releaseBuildID;
	}

	/**
	 * The URL of the Git Reop
	 * @return
	 */
	public String getGitRepoUrl() {
		return gitRepoUrl;
	}
}
