/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.objects;


/**
 * Object which holds the information from a Bad API Issue
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class BadApiIssue {
	// Description of the Issue
	private String pluginVersion;
	// Offending Spout API Version
	private String badSpoutApi;
	// Offending Game API Version
	private String badGameApi;

	/**
	 * The Issue pluginVersion
	 * @return
	 */
	public String getPluginVersion() {
		return pluginVersion;
	}

	/**
	 * The Issue Key
	 * @return
	 */
	public String getBadSpoutApi() {
		return badSpoutApi;
	}

	/**
	 * The Issue Link
	 * @return
	 */
	public String getBadGameApi() {
		return badGameApi;
	}


	/**
	 * Set the Plugin Version this Bad API Issue is about
	 * @param pluginVersion
	 */
	public void setPluginVersion(String pluginVersion) {
		this.pluginVersion = pluginVersion.trim();
	}

	/**
	 * Set the Bad Spout API Version
	 * @param badSpoutApi
	 */
	public void setBadSpoutApi(String badSpoutApi) {
		this.badSpoutApi = badSpoutApi.trim();
	}

	/**
	 * Set the Bad Game API Version
	 * @param badGameApi
	 */
	public void setBadGameApi(String badGameApi) {
		this.badGameApi = badGameApi.trim();
	}

	/**
	 * Should we just kill the plugin without really checking the versions?
	 * @return true if either badSpoutAPI or badGameAPI is not equals ""
	 */
	public boolean justKill() {
		boolean kill = true;
		if (badSpoutApi != "" || badGameApi != "") {
			kill = false;
		}
		return kill;
	}
}

