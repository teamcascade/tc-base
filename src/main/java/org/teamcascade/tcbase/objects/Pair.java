/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.objects;

/**
 * An encapsulation for a pair of values. This class should be used if 2 different values needs to be returned
 * from a method. If you want to return the same value but in different formats use {@link Either}
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$*
 */
public final class Pair<L, R>
{
    private final L left;
    private final R right;

    /**
     * Constructs the pair
     *
     * @param left  the left value
     * @param right the right value
     */
    public Pair(L left, R right)
    {
        this.left = left;
        this.right = right;
    }

    /**
     * @return Returns the left value.
     */
    public L getLeft()
    {
        return left;
    }

    /**
     * @return Returns the right value.
     */
    public R getRight()
    {
        return right;
    }
}
