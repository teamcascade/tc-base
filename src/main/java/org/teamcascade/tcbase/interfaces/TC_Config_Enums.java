/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.interfaces;

import org.spout.api.inventory.ItemStack;
import org.teamcascade.tcbase.exceptions.WrongClassException;

import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * Interface für Configuration Enums
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public interface TC_Config_Enums {
	/**
	 * Override for toString, returns the ENUM as CamelCaseString, eg. MEMORY_CMD => MemoryCmd
	 *
	 * @return s ENUM in CamelCase style
	 */

	@Override
	String toString();

	/**
	 * Returns the Configuration Comment
	 *
	 * @return configComment returns the configuration comment
	 */
	 String getConfigComment();

	/**
	 * Returns the configuration option object, may return @null if comment only
	 *
	 * @return configOption configuration option object
	 */
	 Object getConfigOption();

	/**
	 * Allows to set the configuration option object.
	 * Will check if the class of the object is the same.
	 *
	 * @param configOption
	 */
	 void setConfigOption(Object configOption) throws WrongClassException;

	/**
	 * Workaround for Spout methods DON'T use this in your own classes!!!!!
	 * Use setConfigOption(Object configOption) instead!
	 *
	 * @param configurationOption
	 */
	 void setConfigurationOption(Object configurationOption);

	/**
	 * Methods which give back the configOption as a String
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as string
	 */
	 String getString();

	/**
	 * Methods which give back the configOption as a boolean
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as boolean
	 */
	 boolean getBoolean();

	/**
	 * Methods which give back the configOption as a List
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as list
	 */
	 List getList();

	/**
	 * Methods which give back the configOption as an Integer
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as Integer
	 */
	 Integer getInt();

	/**
	 * Methods which give back the configOption as a Double
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as double
	 */
	 Double getDouble();

	/**
	 * Methods which give back the configOption as a Long
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as Long
	 */
	 Long getLong();

	/**
	 * Methods which give back the configOption as an ItemStack
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as ItemStack
	 */
	 ItemStack getItemStack();

	/**
	 * Methods which give back the configOption as a Vector
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as Vector
	 */
	 Vector getVector();

	/**
	 * Methods which give back the configOption as a List<Boolean>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as List<Boolean>
	 */
	 List<Boolean> getBooleanList();

	/**
	 * Methods which give back the configOption as a List<Byte>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as List<Byte>
	 */
	 List<Byte> getByteList();

	/**
	 * Methods which give back the configOption as a List<Character>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as List<Character>
	 */
	 List<Character> getCharacterList();

	/**
	 * Methods which give back the configOption as a List<Double>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as List<Double>
	 */
	 List<Double> getDoubleList();

	/**
	 * Methods which give back the configOption as a List<Float>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as List<Float>
	 */
	 List<Float> getFloatList();

	/**
	 * Methods which give back the configOption as a List<Integer>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as List<Integer>
	 */
	 List<Integer> getIntegerList();

	/**
	 * Methods which give back the configOption as a List<Long>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as List<Long>
	 */
	 List<Long> getLongList();

	/**
	 * Methods which give back the configOption as a List<Map<String, Object>>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as List<Map<String, Object></Map>>
	 */
	 List<Map<String, Object>> getMapList();

	/**
	 * Methods which give back the configOption as a Map<String,Object>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as Map<String,Object>
	 */
	 Map<String, Object> getMap();

	/**
	 * Methods which give back the configOption as a List<Short>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as List<Short>
	 */
	 List<Short> getShortList();

	/**
	 * Methods which give back the configOption as a List<String>
	 * Note: This can cause exceptions if you don't do it correctly
	 *
	 * @return configOption as List<String>
	 */
	 List<String> getStringList();

}
