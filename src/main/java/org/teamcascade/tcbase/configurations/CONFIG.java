/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.configurations;

import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Pattern;

import org.teamcascade.tcbase.exceptions.WrongClassException;
import org.teamcascade.tcbase.interfaces.TC_Config_Enums;

import org.spout.api.inventory.ItemStack;

/**
 * The config Enum contains the full configuration of the plugin. The Enum will be parsed in order and written to file.
 * <br>
 * You can use \n as a line break but in this case you need to supply the # by yourself, see the example for CONFIG_AUTO_UPDATE
 * <br>
 * If the Enum doesn't contain an Object it will be a comment at that part of the config file.
 * <br>
 * You may want to keep the options DEBUG_LOG_ENABLED, LOG_TO_FILE, CHECK_FOR_UPDATE, CONFIG_AUTO_UPDATE, CONFIG_AUTO_SAVE, CONFIG_LOG_ENABLED
 * and use DEBUG_LOG_ENABLED, LOG_TO_FILE and CONFIG_LOG_ENABLED in your classes for better logging.
 * <br>
 * If you directly change the enum values there is no flagging or automatic saving during running the server.
 * If you change the enums via {@link org.teamcascade.tcbase.helper.config.CommentConfigurationManager#set} the enum is updated, configuration is flagged dirty and
 * if CONFIG_AUTO_SAVE is enabled will force the config to file directly.
 *
 * @todo You need to add all your configuration parameters here.
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */

public enum CONFIG implements TC_Config_Enums {
	// you should really keep the following lines ==>>>
	CONFIG_VERSION("Version of the configuration","1.0"),
	CONFIG_LOG_ENABLED("Enable logging of the config.. this could be lot's of info", true),
	DEBUG_LOG_ENABLED("Enable more logging.. could be messy!", true),
	LOG_TO_FILE("When logging, log into separate file", true),
	LOG_LEVEL("Which LogLevel should be used?","INFO"),
	CHECK_FOR_UPDATE("Do you want to check if there is a new version available?", true),
	CONFIG_AUTO_UPDATE("Should the configuration automatically be updated \n#   when there is a new version available?", true),
	CONFIG_AUTO_SAVE("Should we automatically save any changes issued by commands to disc?", true),
	JIRA_ENABLED("Enable announcing of important issues",true),
	JIRA_ISSUES_ANNOUNCE("Announce important issues to holder of the following permission","tcbase.admin"),
	JIRA_SCHEDULE("Check and announce issues every X minutes",60),
	BUILD_CHECK_ENABLED("Enable announcing of new released builds",true),
	BUILD_CHECK_ANNOUNCE("Announce new builds to holder of the following permission","tcbase.admin"),
	BUILD_CHECK_SCHEDULE("Check and announce issues every X minutes",60),
	API_CHECK_ENABLED("Enable announcing of API issues, this SHOULDN'T be disabled",true),
	API_CHECK_ANNOUNCE("Announce API issues to holder of the following permission during login","tcbase.admin"),
	API_CHECK_SCHEDULE("Check API issues against whitelist every X minutes",60),
	API_CHECK_DISABLE_PLUGIN("Disables the Plugin if not the right versions are used, this SHOULDN'T be disabled",true),
	CONFIG_END("End of Default Configuration\n"),
	CONFIG_END_LINE("####################################################################\n"),
	GENERIC_PLUGIN_START("Begin of Generic Plugin Configuration\n");
	// <<<====== from here on onwards it is all yours don't forget tor replace the ; on top with a ,




	// ==========>>> NO CHANGES BELOW HERE!!!!!


	/**
	 * Used for replacing _
	 */

	private static final Pattern COMPILE = Pattern.compile("_");

	//~--- fields -------------------------------------------------------------


	/**
	 * The configuration Option
	 */

	private Object configOption = null;


	/**
	 * The comment for the configuration file for the configuration option
	 */

	private String configComment;

	//~--- constructors -------------------------------------------------------


	/**
	 * Creating the Comment ENUM
	 *
	 * @param configComment the comment for the configuration file
	 */


	private CONFIG(String configComment) {
		this.configOption = null;
		this.configComment = configComment;
	}


	/**
	 * Creating the ENUM with the correct information
	 *
	 * @param configOption  the configuration option
	 * @param configComment the comment for the configuration file
	 */

	private CONFIG(String configComment, Object configOption) {
		this.configOption = configOption;
		this.configComment = configComment;
	}

	//~--- methods ------------------------------------------------------------


	@Override
	public String toString() {
		return toCamelCase(super.toString());
	}


	/**
	 * Removes _ from the Enum and makes it CamelCase
	 *
	 * @param s ENUM to turn into CamelCase
	 * @return camelCaseString
	 */

	static String toCamelCase(String s) {
		final String[] parts = COMPILE.split(s);
		StringBuffer camelCaseString = new StringBuffer();

		for (String part : parts) {
			camelCaseString.append(toProperCase(part));
		}

		return camelCaseString.toString();
	}


	/**
	 * Turns MEMORY into Memory (aka ProperCase)
	 *
	 * @param s string to turn to ProperCase
	 * @return s string in properCase
	 */

	private static String toProperCase(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}

	//~--- get methods --------------------------------------------------------


	@Override
	public String getConfigComment() {
		return configComment;
	}


	@Override
	public Object getConfigOption() {
		return configOption;
	}

	//~--- set methods --------------------------------------------------------


	@Override
	public void setConfigOption(Object configOption) throws WrongClassException {
		if (configOption.getClass().isInstance(this.configOption)) {
			this.configOption = configOption;
			return;
		}
		throw new WrongClassException(this, configOption);
	}


	@Override
	public void setConfigurationOption(Object configurationOption) {
		configOption = configurationOption;
	}

	@Override
	public String getString() {
		return (String) configOption;
	}

	@Override
	public boolean getBoolean() {
		return (Boolean) configOption;
	}

	@Override
	public List getList() {

		return (List) configOption;
	}

	@Override
	public Integer getInt() {
		return (Integer) configOption;
	}

	@Override
	public Double getDouble() {
		return (Double) configOption;
	}

	@Override
	public Long getLong() {

		return (Long) configOption;
	}

	@Override
	public ItemStack getItemStack() {

		return (ItemStack) configOption;
	}

	@Override
	public Vector getVector() {

		return (Vector) configOption;
	}

	@Override
	public List<Boolean> getBooleanList() {

		return (List<Boolean>) configOption;
	}

	@Override
	public List<Byte> getByteList() {

		return (List<Byte>) configOption;
	}

	@Override
	public List<Character> getCharacterList() {

		return (List<Character>) configOption;
	}


	@Override
	public List<Double> getDoubleList() {

		return (List<Double>) configOption;
	}

	@Override
	public List<Float> getFloatList() {

		return (List<Float>) configOption;
	}

	@Override
	public List<Integer> getIntegerList() {

		return (List<Integer>) configOption;
	}

	@Override
	public List<Long> getLongList() {

		return (List<Long>) configOption;
	}

	@Override
	public List<Map<String, Object>> getMapList() {

		return (List<Map<String, Object>>) configOption;
	}

	@Override
	public Map<String, Object> getMap() {

		return (Map) configOption;
	}

	@Override
	public List<Short> getShortList() {

		return (List<Short>) configOption;
	}

	@Override
	public List<String> getStringList() {

		return (List<String>) configOption;
	}

	}
