/*
 * This file is part of TC_Base.
 *
 * Copyright (c) 2012-2012, TeamCascade <https://www.teamcascade.org/>
 * TC_Base is licensed under the TeamCascade Public License v1.
 *
 * TC_Base is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * In addition, 180 days after any changes are published, you can use the
 * software, incorporating those changes, under the terms of the MIT license,
 * as described in the TeamCascade Public License v1.
 *
 * TC_Base is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License,
 * the MIT license and the TeamCascade Public License v1 along with this program.
 * If not, see <http://www.gnu.org/licenses/> for the GNU Lesser General Public
 * License and see <http://www.teamcascade.org/download/attachments/2523197/LICENSE.txt> for the full license,
 * including the MIT license.
 */
package org.teamcascade.tcbase.events;

import org.spout.api.entity.Player;
import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.event.player.PlayerJoinEvent;
import org.teamcascade.tcbase.objects.TC_Meta;

/**
 * Listener for player events.
 *
 * @author $Author: dredhorse$
 * @version $FullVersion$
 */
public class PlayerEvents implements Listener {

	private TC_Meta meta;

	public PlayerEvents(TC_Meta meta){
		this.meta = meta;
		meta.getLog().debug("Enabled PlayerEvents");
	}

	@EventHandler
	public void onPlayerJoin (PlayerJoinEvent event){
		   if (!event.isCancelled() && meta.getConfig().getConfigEnum("ApiCheckEnabled").getBoolean()) {
			   Player player = event.getPlayer();
			   meta.getLog().debug("PlayerJoinEvent executed");
			 	if (player.hasPermission(meta.getConfig().getConfigEnum("ApiCheckAnnounce").toString())){
					meta.getApiCheck().announceApiIssue(event.getPlayer());
				}
		}
	}
}
