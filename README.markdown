# tc-base

![tc-base][1]

What is tc-base?
------------------------
tc-base is a plugin supplying helpful methods for our other plugins. 

> It will be autodownloaded from our plugins if it doesn't exist and if possible. It does also require an internet connection for a lot of his functions to work!

Components (Spout):
-----------
* Jira Issue Check: Check our Bugtracker about important issues of a plugins and send a message to the admin and / or log it in the console log.
* Release Check : Check if there is a new release of the plugin available and send a message to the admin and / or log it in the console log 
* API Check: Check if Spout, tc-base or the game plugin (for example vanilla) have the required versions to support the plugin, also allow remote killing of the plugin if there is a known issue with a specific API version
* Configuration Handling: Easy configuration handling for out plugins
* Logger: A logger which allows logging to file, displaying all field values of an object and a lot more
* [Metrics][11] : Support for the cool MCStats website
* Custom TaskExecutor: Which allows a call back a method after a task has finished
* Sophisticated File Handling: Unicode Support and Autocloseing of files
* Additional Objects: Objects for Pairs, Either, ValueOf for Enums: 

Components (Vanilla):
-----------
* Sign Helper: Making Sign handling easy
* Death Cause Helper: Figuring out what really killed the player
 

Who is TeamCascade?
-------------------

TeamCascade is the team behind the different projects originating from TeamCascade, we work together on our plugins, sometimes as a team, sometimes as one developer with some helping hands.

![Don Redhorse][20]   |  ![Duck][21]
:--------------------:|:--------------:
 Don Redhorse         |  Dukmeister

Visit our [website][2].
Track and submit issues and bugs on our [issue tracker][3].

Source
------
The latest and greatest source can be found on [BitBucket][4]
Download the latest builds from [Bamboo][5].
You can get maven artifacts from [Artifactory][6].
And you will find the documentation in our [Doxygen Repo][7]

License
-------
tc-base is licensed under [TeamCascade Public License v1][8], but with a provision that files are released under the MIT license 180 days after they are published. Please see the `LICENSE.txt` file for details.

Compiling
---------
tc-base uses Maven to handle its dependencies.

* Install [Maven 2 or 3][9]
* Checkout this repo and run: `mvn clean package install`

Coding Standards
----------------------------------
* If / for / while / switch statement: if (conditions && stuff) {
* Method: public void method(Object paramObject) {
* No Spaces, Tab is preferred!
* No trailing whitespace
* Mid-statement newlines at a 200 column limit
* camelCase, no under_scores except constants
* Constants in full caps with underscores
* Keep the same formatting style everywhere
* You can use the TeamCascadeCodeScheme.xml to implement our coding formatting style in most IDE, [Intellij Idea][10]

Pull Request Standards
----------------------------------
* Sign-off on all commits!
* Finished product must compile successfully with `mvn`!
* No merges should be included in pull requests unless the pull request's purpose is a merge.
* Number of commits in a pull request should be kept to *one commit* and all additional commits must be *squashed*. Pull requests that make multiple changes should have one commit per change.
* Pull requests must include any applicable license headers. (These are generated when running `mvn clean`)

[1]: http://www.teamcascade.org/plugins/servlet/zenfoundation/zenservlet/designs/brands/teamcascade/images/logo1.png
[2]: http://www.teamcascade.org
[3]: http://issues.teamcascade.org
[4]: https://bitbucket.org/teamcascade/tc-base
[5]: http://builds.teamcascade.org
[6]: http://artifacts.teamcascade.org
[7]: http://docs.teamcascade.org
[8]: http://www.teamcascade.org/display/AboutUs/TeamCascade+Public+License
[9]: http://maven.apache.org/download.html
[10]: http://musingsofaprogrammingaddict.blogspot.de/2010/03/import-code-style-settings-into.html
[11]: http://mcstats.org/
[20]: http://www.gravatar.com/avatar/5715022800b638db4951afe80841b314.png?size=55
[21]: https://dl.dropbox.com/u/userid/file.png


